﻿using System;

namespace Utility {
    using UnityEngine;
    using System.Collections;

    public static class CustomInvokeExtensions {
        public static void Invoke ( this MonoBehaviour me, float time, Action theDelegate ) {
            me.StartCoroutine( ExecuteAfterTime( time, theDelegate ) );
        }

        private static IEnumerator ExecuteAfterTime ( float delay, Action theDelegate ) {
            yield return new WaitForSeconds( delay );
            theDelegate();
        }
    }
}
