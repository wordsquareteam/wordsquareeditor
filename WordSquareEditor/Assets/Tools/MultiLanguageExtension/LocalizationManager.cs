﻿using System.Collections.Generic;
using UnityEngine;
using System;

internal static class LocalizationManager {
    internal static event Action<Language> OnChangeLanguage = ( Language param ) => { };

    internal enum Language {
        Ru,
        En
    }

    static TextAsset _textAsset;
    static Dictionary<string, LocalizationDataSet> _textContainer;
    static Language _language = Language.Ru;
    static Language[] _languageId;
    static bool _isReady = false;

    internal static void Init () {
        _textAsset = Resources.Load<TextAsset>( "TextLocalization" );

        string[] data = _textAsset.text.Split( new char[] { '\n' } );
        _textContainer = new Dictionary<string, LocalizationDataSet>();

        _languageId = new Language[2];
        _languageId[0] = Language.Ru;
        _languageId[1] = Language.En;

        for ( int i = 1; i < data.Length; i++ ) {
            string[] row = data[i].Split( new char[] { ',' } );
            if ( row == null ) {
                continue;
            }

            if ( row.Length < 5 ) {
                continue;
            }

            LocalizationDataSet languageStruct = new LocalizationDataSet();
            languageStruct.texts = new string[5];

            languageStruct.texts[0] = row[1];
            languageStruct.texts[1] = row[2];

            _textContainer.Add( row[0], languageStruct );
        }

        _isReady = true;

    }

    internal static string GetText ( string id ) {
        if ( !_textContainer.ContainsKey( id ) ) {
            Debug.LogWarning( "Warning !!!!! нет такого идентификатора " + id );
            return id;
        }
        return _textContainer[id].texts[Array.IndexOf( _languageId, _language )];
    }

    internal static bool GetIsReady () {
        return _isReady;
    }

    internal static Language GetLanguage () {
        return _language;
    }

    internal static String GetLanguageString () {
        return _language.ToString();
    }

    internal static void SetLanguageNoEvent ( Language language ) {
        _language = language;
    }

    internal static void SetLanguage ( Language language ) {
        _language = language;
        OnChangeLanguage( language );
    }
}

