﻿using System.Collections.Generic;
using UnityEngine;
using Common;
using UnityEngine.UI;

namespace EditorLevel {
    public class WordsInput {
        public string word;
        public Toggle objChoose;

        public Color colorWord;

        public List<IndexMatrinx> indexLetters;

        public WordsInput () {
            indexLetters = new List<IndexMatrinx>();
        }
    }
}
