﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using Common;
using System;
using Newtonsoft.Json;
using System.Text;
using Utility;

namespace EditorLevel {

    public class EditorLevelController: MonoBehaviour {

        public InputField nameFile;
        public Text levelName;
        public Text levelString;

        RectTransform vpnWord;
        [SerializeField]
        GameObject cooseWordPrf;
        [SerializeField]
        GameObject matrixLetterObj;
        [SerializeField]
        RectTransform groupList;
        [SerializeField]
        TextAsset _wordsFile;

        int sizeField;
        List<WordsInput> wordsInput;

        RectTransform editorUIMatrix;
        EditorLevelMatrix[,] matrixLetter;
        int coosenWord;
        GameController _GC;
        Quests loadQuest;
        public GameObject windowinfo;
        public Text windowinfoText;
        public GameObject buttonOkInfo;
        public GameObject buttonYesInfo;
        public GameObject buttonNoInfo;

        List<string> taskInfoString;

        bool buttonMiddleClick;
        Vector2 buttonMiddleClickPos;
        Dictionary<string, Dictionary<string, QuestsWordsHintsPair>> wordsHintByLevelName;

        void Start () {
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            coosenWord = -1;

            vpnWord = GameObject.Find( "editor_ui_tool_viewport" ).GetComponent<RectTransform>();

            editorUIMatrix = GameObject.Find( "editor_ui_matrix" ).GetComponent<RectTransform>();

            wordsInput = new List<WordsInput>();

            windowinfo.GetComponent<RectTransform>().anchoredPosition = new Vector2( 0f, 0f );
            windowinfo.SetActive( false );
            taskInfoString = new List<string> { "Add all letters on field", "Level saved", "Enter the file name",
                                                "This file already exist. Do you want rewrite?", "You don't have such level! Check the file name please!"
                                              };
            InitWordHints();

            levelName.text = _GC.editQuestName;
            nameFile.text = _GC.editQuestName;

            loadQuest = _GC.editQuest;
            if ( loadQuest != null ) {
                GenerateLevel();
            }

            vpnWord.anchorMin = new Vector2( 0f, 0f );
            vpnWord.anchorMax = new Vector2( 1f, 1f );
            vpnWord.pivot = new Vector2( 0.5f, 0.5f );
            //contenWord.
        }

        void Update () {
            if ( Input.GetMouseButtonDown( 2 ) ) {
                Debug.Log( "Pressed middle click." );
                buttonMiddleClick = true;
                buttonMiddleClickPos = Input.mousePosition;
            }
            if ( Input.GetMouseButtonUp( 2 ) ) {
                Debug.Log( "Pressed middle click end." );
                buttonMiddleClick = false;
            }

            if ( buttonMiddleClick ) {
                ButtonMiddleClick();
            }

            if ( Input.GetAxis( "Mouse ScrollWheel" ) > 0f ) { // forward
                Vector2 newScale = editorUIMatrix.localScale + new Vector3( 0.1f, 0.1f, 0 );
                if ( newScale.x > 4f ) {
                    newScale = new Vector3( 4f, 4f, 1f );
                }
                editorUIMatrix.localScale = newScale;
            }
            else if ( Input.GetAxis( "Mouse ScrollWheel" ) < 0f ) { // backwards
                Vector2 newScale = editorUIMatrix.localScale - new Vector3( 0.1f, 0.1f, 0 );
                if ( newScale.x < 1f ) {
                    newScale = Vector3.one;
                }
                editorUIMatrix.localScale = newScale;
            }
        }

        void InitWordHints () {
            wordsHintByLevelName = new Dictionary<string, Dictionary<string, QuestsWordsHintsPair>>();
            string[] data = _wordsFile.text.Split( new char[] { '\n' } );

            Dictionary<int, string> indexByLanguage = new Dictionary<int, string>();

            indexByLanguage.Add( 1, LocalizationManager.Language.Ru.ToString() );
            indexByLanguage.Add( 3, LocalizationManager.Language.En.ToString() );

            for ( int i = 1; i < data.Length; i++ ) {
                string[] row = data[i].Split( new char[] { ',' } );
                Dictionary<string, QuestsWordsHintsPair> wordsByLanguageTemp = new Dictionary<string, QuestsWordsHintsPair>();
                for ( int j = 1; j < row.Length; j++ ) {
                    if ( !indexByLanguage.ContainsKey( j ) ) {
                        continue;
                    }

                    List<string> wordsTemp = new List<string>();
                    List<string> wordsHintTemp = new List<string>();

                    string[] line = row[j].Split( new char[] { ';' } );
                    string[] lineHint = row[j + 1].Split( new char[] { ';' } );
                    for ( int z = 0; z < line.Length; z++ ) {
                        string wordAlone = line[z].Replace( "\r\n", "" ).Replace( "\r", "" ).Replace( "\n", "" );
                        wordsTemp.Add( wordAlone );

                        if ( z < lineHint.Length ) {
                            string wordHintAlone = lineHint[z].Replace( "\r\n", "" ).Replace( "\r", "" ).Replace( "\n", "" );
                            wordsHintTemp.Add( wordHintAlone );
                        }
                        else {
                            wordsHintTemp.Add( "" );
                        }
                    }

                    wordsByLanguageTemp.Add( indexByLanguage[j], new QuestsWordsHintsPair( wordsTemp, wordsHintTemp ) );
                }
                wordsHintByLevelName.Add( row[0], wordsByLanguageTemp );
            }
        }

        void ButtonMiddleClick () {
            Vector2 currentMousePosition = Input.mousePosition;
            float scaleFactor = 1f;
            Vector2 diffPosition = ( buttonMiddleClickPos - currentMousePosition ) * new Vector2( scaleFactor, scaleFactor );
            Vector2 newFieldPosition = editorUIMatrix.anchoredPosition - diffPosition;

            if ( newFieldPosition.x < -300f ) {
                newFieldPosition = new Vector2( -300f, newFieldPosition.y );
            }
            else if ( newFieldPosition.x > 300f ) {
                newFieldPosition = new Vector2( 300f, newFieldPosition.y );
            }

            if ( newFieldPosition.y > 0f ) {
                newFieldPosition = new Vector2( newFieldPosition.x, 0f );
            }
            else if ( newFieldPosition.y < -600f ) {
                newFieldPosition = new Vector2( newFieldPosition.x, -600f );
            }

            editorUIMatrix.anchoredPosition = newFieldPosition;
            buttonMiddleClickPos = Input.mousePosition;
        }

        public void ShowWindowinfo ( int num = 0 ) {
            if ( num == 3 ) {

                buttonOkInfo.SetActive( false );
                buttonYesInfo.SetActive( true );
                buttonNoInfo.SetActive( true );
            }
            else {
                buttonOkInfo.SetActive( true );
                buttonYesInfo.SetActive( false );
                buttonNoInfo.SetActive( false );
            }

            windowinfoText.text = taskInfoString[num];
            windowinfo.SetActive( true );
        }

        public void HideWindowinfo () {

            windowinfo.SetActive( false );
        }
        //-----------------------------------------------------------------------------------

        Quests newQuest;
        //string savePath;
        public void SaveLevelJson () {
            newQuest = MakeQuest();

            if ( CanISave( newQuest ) ) {

                if ( levelName.text.Length > 0 ) {

                    string savePath = GetPathSavesToLoad( levelName.text );

                    if ( !File.Exists( savePath ) ) {

                        SaveQuest();

                        //_GC.SaveQuest( savePath, newQuest );
                    }
                    //else
                    //if ( File.Exists( savePath ) && loadQuest != null ) {

                    //    ShowWindowinfo( 3 );
                    //    //_GC.SaveQuest( savePath, newQuest );
                    //    //Debug.Log( "Are you shure you wont rewrite file!" );
                    //}
                    else {

                        ShowWindowinfo( 3 );
                        //SaveQuest();
                        //_GC.SaveQuest( savePath, newQuest );
                    }
                }
                else {
                    ShowWindowinfo( 2 );
                }
            }
            else {

                ShowWindowinfo();
            }

        }
        public void SaveQuest () {

            SaveQuest( levelName.text, newQuest );
            ShowWindowinfo( 1 );
        }

        bool CanISave ( Quests newQuest ) {

            if ( newQuest == null ) {

                return false;
            }
            else if ( newQuest.words == null || newQuest.indexLetters == null ) {

                return false;
            }
            else if ( !newQuest.words.ContainsKey( LocalizationManager.Language.Ru.ToString() ) ) {
                return false;
            }
            else if ( newQuest.words[LocalizationManager.Language.Ru.ToString()].Count != newQuest.indexLetters.Count ) {
                return false;
            }
            else {

                for ( int i = 0; i < newQuest.words[LocalizationManager.Language.Ru.ToString()].Count; i++ ) {

                    if ( newQuest.words[LocalizationManager.Language.Ru.ToString()][i].Length > newQuest.indexLetters[i].indexLetters.Count ) {

                        return false;
                    }
                }
            }

            return true;
        }
        //------------------------------------------------------------------------------------
        public void LoadLevelJson () {

            string savePath = GetPathSavesToLoad( levelName.text );

            if ( File.Exists( savePath ) ) {

                string saveObject = File.ReadAllText( savePath );
                string json = AuxiliaryTools.GetJsonFromEncrypterString( saveObject );

                //JsonData jsonPlayer = JsonMapper.ToObject( json );

                loadQuest = JsonConvert.DeserializeObject<Quests>( json );
                loadQuest.nameLevel = levelName.text;

                //loadQuest = new Quests( levelName.text );
                //loadQuest.sizeField = int.Parse( jsonPlayer["sizeField"].ToString() );

                //loadQuest.words = new Dictionary<string, List<string>>();
                //foreach ( KeyValuePair<string, JsonData> item in jsonPlayer["words"] ) {
                //    List<string> wordsTemp = new List<string>();
                //    for ( int i = 0; i < item.Value.Count; i++ ) {
                //        wordsTemp.Add( item.Value[i].ToString() );
                //    }

                //    loadQuest.words.Add( item.Key, wordsTemp );
                //}

                //////loadQuest.words = new List<string>();
                ////for ( int i = 0; i < jsonPlayer[ "words" ].Count; i++ ) { // считывание данных из структуры
                ////    loadQuest.words.Add( jsonPlayer[ "words" ][ i ].ToString() );
                ////}

                //int indexI;
                //int indexJ;

                //loadQuest.indexLetters = new List<ArrayIndexMatrinx>();

                //for ( int i = 0; i < jsonPlayer["indexLetters"].Count; i++ ) { // считывание данных из структуры

                //    loadQuest.indexLetters.Add( new ArrayIndexMatrinx() );
                //    for ( int j = 0; j < jsonPlayer["indexLetters"][i]["indexLetters"].Count; j++ ) {

                //        indexI = int.Parse( jsonPlayer["indexLetters"][i]["indexLetters"][j]["indI"].ToString() );
                //        indexJ = int.Parse( jsonPlayer["indexLetters"][i]["indexLetters"][j]["indJ"].ToString() );

                //        loadQuest.indexLetters[i].AddNewLetterIndex( new IndexMatrinx( indexI, indexJ ) );
                //    }
                //}

                GenerateLevel();
            }
            else {
                ShowWindowinfo( 4 );
            }
        }

        void GenerateLevel () {

            levelString.text = "";

            InputField inputField = levelString.gameObject.GetComponentInParent<InputField>();
            inputField.text = "";

            for ( int i = 0; i < loadQuest.words[LocalizationManager.Language.Ru.ToString()].Count; i++ ) {
                inputField.text += loadQuest.words[LocalizationManager.Language.Ru.ToString()][i];
                inputField.text += " ";
            }

            GenerateMatrix( loadQuest.sizeField );
            InputingString();

            int indexI;
            int indexJ;

            for ( int i = 0; i < loadQuest.indexLetters.Count; i++ ) {

                coosenWord = i;

                if ( i >= loadQuest.words[LocalizationManager.Language.Ru.ToString()].Count ) {
                    continue;
                }

                for ( int j = 0; j < loadQuest.indexLetters[i].indexLetters.Count; j++ ) {
                    indexI = loadQuest.indexLetters[i].indexLetters[j].indI;
                    indexJ = loadQuest.indexLetters[i].indexLetters[j].indJ;

                    if ( indexI >= loadQuest.sizeField || indexJ >= loadQuest.sizeField ) {
                        continue;
                    }
                    FillMatrix( new IndexMatrinx( indexI, indexJ ) );
                }
            }

            coosenWord = -1;
        }

        public void InputingString () {

            coosenWord = -1;

            string words = levelString.text.ToUpper();

            string[] split = words.Split( new char[] { ' ', ',', '.', ':', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' } );

            for ( int i = 0; i < wordsInput.Count; i++ ) {

                Destroy( wordsInput[i].objChoose.gameObject );
            }

            wordsInput.Clear();
            wordsInput = new List<WordsInput>();

            int maxLength = 0;
            int count = 0;
            foreach ( string s in split ) {

                if ( s.Trim() != "" && s.Length >= 1 ) {

                    maxLength += s.Length;

                    wordsInput.Add( new WordsInput() );
                    wordsInput[wordsInput.Count - 1].word = s;

                    GameObject goWord = Instantiate( cooseWordPrf, Vector2.zero, Quaternion.identity );
                    goWord.gameObject.name = cooseWordPrf.name + "_" + count;
                    goWord.transform.SetParent( groupList, false );
                    goWord.GetComponent<RectTransform>().anchoredPosition = new Vector2( 0f, ( -1f * 104f * count ) );
                    goWord.GetComponentInChildren<Text>().text = s;
                    goWord.GetComponentInChildren<EditorLevelChoose>().SetStart( count, this );
                    goWord.GetComponent<Toggle>().group = groupList.gameObject.GetComponent<ToggleGroup>();

                    wordsInput[wordsInput.Count - 1].objChoose = goWord.GetComponent<Toggle>();
                    wordsInput[wordsInput.Count - 1].indexLetters = new List<IndexMatrinx>();

                    wordsInput[wordsInput.Count - 1].colorWord =
                        new Color( UnityEngine.Random.Range( 0, 0.5f ), UnityEngine.Random.Range( 0, 0.5f ), UnityEngine.Random.Range( 0, 0.5f ) );

                    count++;
                }
            }

            groupList.sizeDelta = new Vector2( groupList.sizeDelta.x, 104f * count );

            int newSizeField = GetSizeField( split );
            if ( newSizeField > 0 ) {
                sizeField = newSizeField;

                GenerateMatrix( sizeField );
            }
        }

        int GetSizeField ( string[] split ) {
            int maxLength = 0;

            foreach ( string s in split ) {
                maxLength += s.Length;
            }
            int newSizeField = 0;

            if ( maxLength > 0 ) {
                float root2 = Mathf.Sqrt( maxLength );
                newSizeField = Mathf.CeilToInt( root2 );
            }

            return newSizeField;
        }

        void GenerateMatrix ( int newSize ) {

            if ( matrixLetter != null ) {

                if ( matrixLetter.Length > 0 ) {

                    foreach ( EditorLevelMatrix m in matrixLetter ) {

                        Destroy( m.gameObject );
                    }
                }
            }

            matrixLetter = new EditorLevelMatrix[newSize, newSize];

            float stepX = 60f;
            int halpSizeField = newSize / 2;

            Vector2 startPos = new Vector2( -1 * stepX * halpSizeField, 0f );

            for ( int i = 0; i < newSize; i++ ) {

                for ( int j = 0; j < newSize; j++ ) {

                    GameObject goMatrix = ( GameObject ) Instantiate( matrixLetterObj, Vector2.zero, Quaternion.identity );
                    goMatrix.name = matrixLetterObj.name + "_" + i + "_" + j;
                    goMatrix.transform.SetParent( editorUIMatrix, false );
                    goMatrix.GetComponent<RectTransform>().anchoredPosition = new Vector2( startPos.x + 60f * j, startPos.y - 60f * i );
                    matrixLetter[i, j] = goMatrix.GetComponent<EditorLevelMatrix>();
                    matrixLetter[i, j].SetStart( new IndexMatrinx( i, j ), this );
                }
            }
        }

        public void ChooseWord ( int numOfChoose ) {

            for ( int i = 0; i < wordsInput.Count; i++ ) {

                if ( numOfChoose != i ) {

                    wordsInput[i].objChoose.isOn = false;
                }
            }
            coosenWord = numOfChoose;
            //wordsInput[ numOfChoose ].objChoose.isOn = true;
        }

        public void FillMatrix ( IndexMatrinx indexIJ ) {

            if ( coosenWord != -1 ) {

                int number = wordsInput[coosenWord].indexLetters.Count;

                if ( number < wordsInput[coosenWord].word.Length ) {

                    if ( IsCanAddWord( number, indexIJ ) ) {

                        if ( indexIJ.indI >= matrixLetter.Length / 2 || indexIJ.indJ >= matrixLetter.Length / 2 ) {
                            return;
                        }

                        matrixLetter[indexIJ.indI, indexIJ.indJ].ChooseWord( number,
                                wordsInput[coosenWord].word[number].ToString(),
                                wordsInput[coosenWord].colorWord );

                        wordsInput[coosenWord].indexLetters.Add( indexIJ );

                        if ( wordsInput[coosenWord].indexLetters.Count >= wordsInput[coosenWord].word.Length ) {

                            wordsInput[coosenWord].objChoose.GetComponent<EditorLevelChoose>().SetDone();
                        }
                    }
                }
                else {
                    //Debug.Log( "111111" );
                }
            }
        }

        public void UnFillMatrix ( IndexMatrinx indexIJ ) {

            if ( coosenWord != -1 ) {

                int number = wordsInput[coosenWord].indexLetters.Count;

                if ( number > 0 ) {

                    if ( wordsInput[coosenWord].indexLetters[number - 1].indI == indexIJ.indI &&
                            wordsInput[coosenWord].indexLetters[number - 1].indJ == indexIJ.indJ ) {

                        matrixLetter[indexIJ.indI, indexIJ.indJ].UnChooseWord();
                        wordsInput[coosenWord].indexLetters.RemoveAt( number - 1 );

                        wordsInput[coosenWord].objChoose.GetComponent<EditorLevelChoose>().SetNotDone();
                    }
                }
            }
        }

        bool IsCanAddWord ( int number, IndexMatrinx indexIJ ) {

            if ( number == 0 ) {
                return true;
            }
            else if ( Mathf.Abs( wordsInput[coosenWord].indexLetters[number - 1].indI - indexIJ.indI ) <= 1 &&
                      Mathf.Abs( wordsInput[coosenWord].indexLetters[number - 1].indJ - indexIJ.indJ ) <= 1 ) {

                return true;
            }
            else {
                return false;
            }
        }

        public void StartEditPlay () {

            Quests newQuest = MakeQuest();
            _GC.SetEditQuest( newQuest );

            if ( CanISave( newQuest ) ) {
                SceneManager.LoadScene( "level" );
            }
            else {
                ShowWindowinfo();
            }
        }

        public Quests MakeQuest () {

            if ( wordsInput.Count > 0 && sizeField != 0 ) {

                Quests newQuest;
                newQuest = new Quests( levelName.text );
                newQuest.sizeField = sizeField;

                string[] valuesAsArray = Enum.GetNames( typeof( LocalizationManager.Language ) );
                for ( int i = 0; i < valuesAsArray.Length; i++ ) {
                    if ( newQuest.words.ContainsKey( valuesAsArray[i] ) ) {
                        continue;
                    }

                    newQuest.words.Add( valuesAsArray[i], new List<string>() );
                }

                if ( wordsHintByLevelName.ContainsKey( levelName.text ) ) {
                    for ( int i = 0; i < valuesAsArray.Length; i++ ) {
                        string languade = valuesAsArray[i];
                        if ( wordsHintByLevelName[levelName.text].ContainsKey( languade ) ) {

                            if ( newQuest.wordsHints.ContainsKey( languade ) ) {
                                newQuest.wordsHints[languade] = wordsHintByLevelName[levelName.text][languade].wordsHints;
                                newQuest.words[languade] = wordsHintByLevelName[levelName.text][languade].words;
                            }
                            else {
                                newQuest.wordsHints.Add( languade, wordsHintByLevelName[levelName.text][languade].wordsHints );
                                newQuest.words.Add( languade, wordsHintByLevelName[levelName.text][languade].words );
                            }
                        }
                    }
                }

                ArrayIndexMatrinx arrayIndexMatrinx;

                for ( int i = 0; i < wordsInput.Count; i++ ) {
                    if ( wordsHintByLevelName.ContainsKey( levelName.text ) ) {
                        if ( i >= newQuest.words[LocalizationManager.Language.Ru.ToString()].Count ) {
                            newQuest.words[LocalizationManager.Language.Ru.ToString()].Add( wordsInput[i].word );
                        }
                        else {
                            newQuest.words[LocalizationManager.Language.Ru.ToString()][i] = wordsInput[i].word;
                        }

                        if ( i >= newQuest.wordsHints[LocalizationManager.Language.Ru.ToString()].Count ) {
                            newQuest.wordsHints[LocalizationManager.Language.Ru.ToString()].Add( "" );
                        }
                    }
                    else {
                        foreach ( KeyValuePair<string, List<string>> item in newQuest.words ) {
                            if ( i >= newQuest.words[item.Key] .Count ) {
                                newQuest.words[item.Key].Add( wordsInput[i].word );
                            }

                            if ( i >= newQuest.wordsHints[item.Key].Count ) {
                                newQuest.wordsHints[item.Key].Add( "" );
                            }
                        }
                    }

                    //newQuest.words.Add( wordsInput[ i ].word );
                    arrayIndexMatrinx = new ArrayIndexMatrinx();

                    for ( int j = 0; j < wordsInput[i].indexLetters.Count; j++ ) {

                        arrayIndexMatrinx.AddNewLetterIndex( wordsInput[i].indexLetters[j] );
                    }
                    newQuest.indexLetters.Add( arrayIndexMatrinx );
                }

                return newQuest;
            }

            return null;
        }

        public void UpdateAllQuests () {

            //return;
            string path = Application.dataPath + "/StreamingAssets/category_quest.json";
            string saveObject = File.ReadAllText( path );
            string json = AuxiliaryTools.GetJsonFromEncrypterString( saveObject );

            CatigoriesMainAndDailyQuests catigoriesMainAndDailyQuests = JsonConvert.DeserializeObject<CatigoriesMainAndDailyQuests>( json );

            for ( int i = 0; i < catigoriesMainAndDailyQuests.catigoriesQuests.Count; i++ ) {
                for ( int j = 0; j < catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest.Count; j++ ) {
                    string levelName = catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest[j];

                    if ( wordsHintByLevelName.ContainsKey( levelName ) ) {
                        Quests loadQuestTemp = File.Exists( GetPathSavesToLoad( levelName ) ) ? GetSerializQuest( levelName ) : CreateTempQuest( levelName,
                                               wordsHintByLevelName[levelName][LocalizationManager.Language.Ru.ToString()] );

                        UpdateWordsInQuest( levelName, loadQuestTemp, wordsHintByLevelName[levelName] );
                    }
                    else {
                        Debug.LogWarning( "No  such level in google doc!!  " + levelName );
                    }
                }
            }

            for ( int i = 0; i < catigoriesMainAndDailyQuests.daylyPuzzleQuest.Count; i++ ) {
                string levelName = catigoriesMainAndDailyQuests.daylyPuzzleQuest[i];

                if ( wordsHintByLevelName.ContainsKey( levelName ) ) {
                    Quests loadQuestTemp = File.Exists( GetPathSavesToLoad( levelName ) ) ? GetSerializQuest( levelName ) : CreateTempQuest( levelName,
                                           wordsHintByLevelName[levelName][LocalizationManager.Language.Ru.ToString()] );

                    UpdateWordsInQuest( levelName, loadQuestTemp, wordsHintByLevelName[levelName] );

                }
                else {
                    Debug.LogWarning( "No  such daily puzzle in google doc!!!!  " + levelName );
                }
            }
        }

        Quests CreateTempQuest ( string levelName, QuestsWordsHintsPair questsWordsHintsPair ) {
            Quests loadQuestTemp = new Quests( "" );

            int maxLength = 0;
            for ( int i = 0; i < questsWordsHintsPair.words.Count; i++ ) {
                maxLength += questsWordsHintsPair.words[i].Length;
            }

            float root2 = Mathf.Sqrt( maxLength );
            int newSizeField = Mathf.CeilToInt( root2 );
            loadQuestTemp.sizeField = newSizeField;

            return loadQuestTemp;
        }

        Quests GetSerializQuest ( string levelName ) {

            string savePath = GetPathSavesToLoad( levelName );
            string saveObject = File.ReadAllText( savePath );
            string json = AuxiliaryTools.GetJsonFromEncrypterString( saveObject );

            //JsonData jsonPlayer = JsonMapper.ToObject( json );

            Quests loadQuestTemp = JsonConvert.DeserializeObject<Quests>( json );

            //loadQuestTemp.sizeField = int.Parse( jsonPlayer["sizeField"].ToString() );
            //int indexI;
            //int indexJ;
            //loadQuestTemp.indexLetters = new List<ArrayIndexMatrinx>();

            //for ( int i = 0; i < jsonPlayer["indexLetters"].Count; i++ ) { // считывание данных из структуры

            //    loadQuestTemp.indexLetters.Add( new ArrayIndexMatrinx() );
            //    for ( int j = 0; j < jsonPlayer["indexLetters"][i]["indexLetters"].Count; j++ ) {

            //        indexI = int.Parse( jsonPlayer["indexLetters"][i]["indexLetters"][j]["indI"].ToString() );
            //        indexJ = int.Parse( jsonPlayer["indexLetters"][i]["indexLetters"][j]["indJ"].ToString() );

            //        loadQuestTemp.indexLetters[i].AddNewLetterIndex( new IndexMatrinx( indexI, indexJ ) );
            //    }
            //}

            return loadQuestTemp;
        }

        void UpdateWordsInQuest ( string levelName, Quests quest, Dictionary<string, QuestsWordsHintsPair> questsWordsHintsPair ) {

            Quests loadQuestFixed = new Quests( "" );
            string[] words = new string[quest.words[LocalizationManager.Language.Ru.ToString()].Count];
            for ( int i = 0; i < quest.words[LocalizationManager.Language.Ru.ToString()].Count; i++ ) {
                words[i] = quest.words[LocalizationManager.Language.Ru.ToString()][i];
            }

            loadQuestFixed.sizeField = GetSizeField( words );

            loadQuestFixed.words = new Dictionary<string, List<string>>();
            loadQuestFixed.wordsHints = new Dictionary<string, List<string>>();

            foreach ( var item in questsWordsHintsPair ) {
                List<string> wordsTemp = new List<string>();
                List<string> wordsHintTemp = new List<string>();
                for ( int i = 0; i < item.Value.words.Count; i++ ) {
                    string str = item.Value.words[i].Trim();
                    wordsTemp.Add( str );
                    str = item.Value.wordsHints[i].Trim();
                    wordsHintTemp.Add( str );
                }
                loadQuestFixed.words.Add( item.Key, wordsTemp );
                loadQuestFixed.wordsHints.Add( item.Key, wordsHintTemp );
            }

            int indexI;
            int indexJ;

            loadQuestFixed.indexLetters = new List<ArrayIndexMatrinx>();

            for ( int i = 0; i < quest.indexLetters.Count; i++ ) { // считывание данных из структуры

                loadQuestFixed.indexLetters.Add( new ArrayIndexMatrinx() );
                for ( int j = 0; j < quest.indexLetters[i].indexLetters.Count; j++ ) {

                    indexI = quest.indexLetters[i].indexLetters[j].indI;
                    indexJ = quest.indexLetters[i].indexLetters[j].indJ;

                    loadQuestFixed.indexLetters[i].AddNewLetterIndex( new IndexMatrinx( indexI, indexJ ) );
                }
            }

            SaveQuest( levelName, loadQuestFixed );
        }

        #region quest
        public void SaveQuest ( string levelName, Quests saveQuest ) {
            string json = JsonConvert.SerializeObject( saveQuest );

            string encryptText = AuxiliaryTools.GetEncryptString( json );

#if UNITY_EDITOR
            string savePathDecipher = Application.dataPath + "/JsonDatasetDecipher/" + levelName + ".json";
            File.WriteAllText( savePathDecipher, json, Encoding.UTF8 );
#endif
            string savePathTemp = GetPathSavesToLoad( levelName );
            File.WriteAllText( savePathTemp, encryptText, Encoding.UTF8 );
        }
        #endregion

        string GetPathSavesToLoad ( string levelNameTmp ) {
            return Application.dataPath + "/StreamingAssets/" + levelNameTmp + ".json";
        }
    }
}