﻿using UnityEngine;
using System.Collections;
using Common;

namespace EditorLevel {
    public class EditorLevelMatrix: MonoBehaviour {

        UnityEngine.UI.Image imgCell;
        UnityEngine.UI.Text textCell;

        int indexLetter;

        EditorLevelController _LEC;

        IndexMatrinx indexIJ;

        void Start () {
        }

        public void SetStart ( IndexMatrinx newIndexIJ, EditorLevelController newLEC ) {

            indexIJ = new IndexMatrinx();
            indexIJ.indI = newIndexIJ.indI;
            indexIJ.indJ = newIndexIJ.indJ;
            imgCell = this.gameObject.GetComponentInChildren<UnityEngine.UI.Image>();
            textCell = this.gameObject.GetComponentInChildren<UnityEngine.UI.Text>();

            indexLetter = -1;

            _LEC = newLEC;
        }

        void Update () {
        }

        public void ChooseWord ( int newIndexLetter, string newLetter, Color newColor ) {

            imgCell.color = newColor;
            indexLetter = newIndexLetter;
            textCell.text = newLetter;
        }

        public void UnChooseWord () {

            indexLetter = -1;
            textCell.text = "";
            imgCell.color = new Color( 0f, 0f, 0f );
        }

        public void ClkToChoose () {

            if ( indexLetter <= -1 ) {

                _LEC.FillMatrix( indexIJ );
            }
            else {
                _LEC.UnFillMatrix( indexIJ );
            }
        }
    }
}
