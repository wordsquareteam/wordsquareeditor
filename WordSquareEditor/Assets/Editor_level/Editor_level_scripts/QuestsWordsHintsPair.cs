﻿using System.Collections.Generic;

namespace EditorLevel {
    public struct QuestsWordsHintsPair {
        public List<string> words;
        public List<string> wordsHints;

        public QuestsWordsHintsPair ( List<string> words, List<string> wordsHints ) {
            this.words = words;
            this.wordsHints = wordsHints;
        }
    }
}
