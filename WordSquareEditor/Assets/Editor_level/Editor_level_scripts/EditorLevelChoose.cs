﻿using UnityEngine;
using System.Collections;

namespace EditorLevel {
    public class EditorLevelChoose: MonoBehaviour {

        int numOfChoose;

        EditorLevelController _ELC;

        public UnityEngine.UI.Image doneText;

        void Start () {
        }

        public void SetStart ( int newNumOfChoose, EditorLevelController editorLevelController, bool setdone = false ) {

            numOfChoose = newNumOfChoose;
            _ELC = editorLevelController;

            if ( setdone ) {
                SetDone();
            }
            else {
                SetNotDone();
            }
        }

        void Update () {
        }

        public void SetDone () {

            doneText.enabled = true;
        }

        public void SetNotDone () {

            doneText.enabled = false;
        }

        public void ChooseWord () {

            bool isOn = this.gameObject.GetComponent<UnityEngine.UI.Toggle>().isOn;

            if ( isOn ) {

                _ELC.ChooseWord( numOfChoose );
            }
        }
    }
}
