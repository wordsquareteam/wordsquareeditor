﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Puzzle {
    public class LetterDoneButtonI: MonoBehaviour {
        public event Action<int> OnClickButtinI = ( int patram1 ) => { };

        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Button buttinI;

        int indexWord;

        private void Awake () {
            buttinI.onClick.AddListener( ClickButtinI );
        }

        internal void SetStart ( int indexWord ) {
            this.indexWord = indexWord;
        }

        void ClickButtinI () {
            OnClickButtinI( indexWord );
        }

        internal void SetPosition ( Vector2 newPosition ) {
            myRectTransform.anchoredPosition = newPosition;
        }
    }
}
