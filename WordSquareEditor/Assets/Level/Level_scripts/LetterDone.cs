﻿using UnityEngine;
using UnityEngine.UI;

namespace Puzzle {
    public class LetterDone: MonoBehaviour {
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Image _image;
        [SerializeField]
        Text _text;

        public RectTransform MyRectTransform {
            get {
                return myRectTransform;
            }
        }

        internal void SetLetter ( string letter ) {
            _text.text = letter;
        }
        internal void SetEnabledText ( bool enabled ) {
            _text.enabled = enabled;
        }

        internal void SetEnabledImage ( bool enabled ) {
            _image.enabled = enabled;
        }

        internal void SetPosition ( Vector2 newPosition ) {
            myRectTransform.anchoredPosition = newPosition;
        }
    }
}
