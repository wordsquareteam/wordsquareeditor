﻿using UnityEngine;
using System.Collections;

namespace Puzzle {
    public class LetterDoneFly: MonoBehaviour {

        CanvasGroup myCanvsGroup;

        RectTransform myRecTransform;

        Vector2 startPosition;

        Vector2 endPosition;

        bool preFly;
        bool visible;

        public void SetStart ( string letter, Vector2 newStartPosition ) {

            startPosition = newStartPosition;

            myRecTransform = this.gameObject.GetComponent<RectTransform>();
            myRecTransform.anchoredPosition = startPosition;

            preFly = false;

            myCanvsGroup = this.gameObject.GetComponent<CanvasGroup>();
            myCanvsGroup.alpha = 0;
            visible = true;

            this.gameObject.transform.GetComponentInChildren<UnityEngine.UI.Text>().text = letter;
            this.gameObject.transform.GetComponentInChildren<UnityEngine.UI.Image>().color =
                new Color( UnityEngine.Random.Range( 0.5f, 1f ), UnityEngine.Random.Range( 0.5f, 1f ), UnityEngine.Random.Range( 0.5f, 1f ) );
        }

        void Update () {

            if ( !visible ) {

                if ( myCanvsGroup.alpha >= 1f ) {
                    visible = false;
                }
                else {

                    myCanvsGroup.alpha += 0.05f;
                }
            }

            if ( preFly ) {

                if ( Vector2.Distance( myRecTransform.anchoredPosition, endPosition ) <= 3500f * Time.deltaTime ) {
                    preFly = false;

                    myRecTransform.anchoredPosition = endPosition;
                }
                else {
                    myRecTransform.anchoredPosition = Vector2.MoveTowards( myRecTransform.anchoredPosition, endPosition, 3500f * Time.deltaTime );
                }
            }

            //if ( postFly ) {

            //    if ( Vector2.Distance( myRecTransform.anchoredPosition, endPosition ) <= 0.1f ) {
            //        preFly = false;
            //        postFly = true;
            //    }
            //    else {
            //        myRecTransform.anchoredPosition = Vector2.Lerp( myRecTransform.anchoredPosition, endPosition, 0.1f );
            //    }
            //}
        }

        public void StartFly ( Vector2 newEndPos, float timeFly ) {

            endPosition = newEndPos;
            myRecTransform.anchoredPosition = startPosition;
            StartCoroutine( FlyLetter( timeFly ) );
        }

        IEnumerator FlyLetter ( float timeWait ) {

            yield return new WaitForSeconds( timeWait );
            visible = false;
            preFly = true;
        }

        public void RestartHide () {
            myRecTransform.anchoredPosition = startPosition;
            visible = true;
            preFly = false;
            myCanvsGroup.alpha = 0;
        }
    }
}