﻿using DG.Tweening;
using Menu;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Common;
using UnityEngine.Analytics;
using Utility;

namespace Puzzle {
    public class PuzzleController: MonoBehaviour {
        internal event Action OnClickButtonBack = () => { };
        internal event Action<Quests> OnStartQuest = ( Quests param ) => { };
        internal event Action OnWinQuest = () => { };
        internal event Action<int, int> OnStartHideLetter = ( int param1, int param2 ) => { };
        internal event Action<string> OnWrongWord = ( string param ) => { };
        internal event Action<bool> OnActivePuzzle = ( bool param ) => { };

        [SerializeField]
        RectTransform _letterUIField;
        [SerializeField]
        RectTransform _letterDoneUIContanerFly;
        [SerializeField]
        RectTransform _letterDoneUIContaner;
        [SerializeField]
        RectTransform _letterUIParticles;
        [SerializeField]
        Text _inputText;
        [SerializeField]
        AudioSource _myAudioSource;
        [SerializeField]
        CanvasGroup _canvasGroup;
        [SerializeField]
        UnityEngine.EventSystems.EventTrigger _letterClick;
        [SerializeField]
        RectTransform backLetterDone;

        GameController _GC;
        WinUI _winUI;
        LockUI _lockUI;
        LettersPool _lettersPool;
        Animator _myAnimator;

        string nameLevel = "";
        List<List<LettersLevel>> _listLetters;
        int _sizeI; // row
        int _sizeJ; // collumn

        FieldLettersParameters parametersLettersField;
        FieldLettersParameters parametersLettersDone;

        List<string> _lettersTask;
        List<ArrayIndexMatrinx> _checkLetters;
        List<List<LetterDone>> _arrayWords;
        List<List<bool>> _arrayWordsBool;
        List<LetterDoneButtonI> letterDoneButtonsI;

        int _mostLenghtWord;
        float _lowerEdge;

        bool _win;
        public bool Win {
            get {
                return _win;
            }
        }

        bool _isMouseDown;
        string _stringTask;
        List<bool> _doneTask;
        Vector2 _plusEdge;
        Vector2 _minusEdge;
        List<IndexMatrinx> _downLetters;
        int _loseCount;
        string[] _loseAudio;
        Quests _quest;
        int _numHintQuest;
        bool _contHideletters;
        Coroutine _puzzleUpdate;
        bool _isInit;
        bool _isActivePuzzle;
        PuzzleType _puzzelType;
        TrailSystemMouse trailSystem;

        List<LettersLevel> hideLettes;

        private void Awake () {
            hideLettes = new List<LettersLevel>();

            _win = false;
            _isMouseDown = false;

            _downLetters = new List<IndexMatrinx>();

            //_levelUIGroup = this.gameObject;
            _winUI = FindObjectOfType<WinUI>();
            _lockUI = FindObjectOfType<LockUI>();
            _listLetters = new List<List<LettersLevel>>();
            _lettersPool = FindObjectOfType<LettersPool>();
            trailSystem = FindObjectOfType<TrailSystemMouse>();

            UnityEngine.EventSystems.EventTrigger.Entry entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener( ( data ) => {
                ClickOnFieldDown( ( PointerEventData ) data );
            } );
            _letterClick.triggers.Add( entry );

            entry = new UnityEngine.EventSystems.EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener( ( data ) => {
                ClickOnFieldUp( ( PointerEventData ) data );
            } );
            _letterClick.triggers.Add( entry );
        }

        internal void Init () {
            if ( _isInit ) {
                return;
            }

            _isInit = true;
            RectTransform rectTransform = GetComponent<RectTransform>();
            _lowerEdge = rectTransform.rect.height / 2;
            _plusEdge = new Vector2( rectTransform.rect.width / 2 - 100f, _lowerEdge - 100f );
            _minusEdge = new Vector2( ( rectTransform.rect.width / 2 - 100f ) * ( -1 ), ( _lowerEdge - 300f ) * ( -1 ) );

            _loseAudio = new string[3] { "wrongword1", "wrongword2", "wrongword3" };
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();
        }

        internal PuzzleType GetPuzzelType () {
            return _puzzelType;
        }

        internal Quests GetQuest () {
            return _quest;
        }

        internal Vector2 GetLettersLevelPositionByIndex ( int indexI, int indexJ ) {
            if ( indexI >= _listLetters.Count ) {
                return Vector2.zero;
            }

            if ( indexJ >= _listLetters[indexI].Count  ) {
                return Vector2.zero;
            }

            return _listLetters[indexI][indexJ].letterCntr.MyRectTransform.anchoredPosition;
        }

        internal int GetNotDoneTask () {
            for ( int i = 0; i < _doneTask.Count; i++ ) {
                if ( !_doneTask[i] ) {
                    return i;
                }
            }

            return -1;
        }

        internal string GetLettersTaskByIndex ( int indexLettersTask ) {
            return _lettersTask[indexLettersTask];
        }

        internal bool GetIsActivePuzzle () {
            return _isActivePuzzle;
        }

        void ClickOnFieldDown ( PointerEventData data ) {
            _isMouseDown = true;
            trailSystem.StartTrial();
        }

        void ClickOnFieldUp ( PointerEventData data ) {
            if ( !_isMouseDown ) {
                return;
            }

            _isMouseDown = false;
            StartShowLetters();
        }

        void StopPuzzleUpdate () {
            if ( _puzzleUpdate != null ) {
                StopCoroutine( _puzzleUpdate );
            }
        }

        void StartPuzzleUpdate () {
            StopPuzzleUpdate();

            _puzzleUpdate = StartCoroutine( PuzzleUpdateCoroutine() );
        }

        internal void SetActivePuzzle ( bool activePuzzle ) {
            _isActivePuzzle = activePuzzle;
            _letterUIField.gameObject.SetActive( activePuzzle );

            OnActivePuzzle( activePuzzle );
        }

        IEnumerator PuzzleUpdateCoroutine () {
            for ( ; ; ) {
                PuzzleUpdate();
                yield return null;
            }
        }

        void PuzzleUpdate () {
            if ( _win ) {
                return;
            }

            if ( !_isActivePuzzle ) {
                return;
            }

            Vector2 mousePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( _letterUIParticles, Input.mousePosition, Camera.main, out mousePosition );

            if ( IsEndField( mousePosition ) && _isMouseDown ) {
                StartShowLetters();
            }
        }

        void SubscribeLetter ( int indexI, int indexJ ) {
            _listLetters[indexI][indexJ].letterCntr.OnDownLetter += OnDownLetter;
            _listLetters[indexI][indexJ].letterCntr.OnUpLetter += OnUpLetter;
            _listLetters[indexI][indexJ].letterCntr.OnEnterLetter += OnEnterLetter;
            _listLetters[indexI][indexJ].letterCntr.OnAddNewLetter += AddNewLetter;
        }

        void UnSubscribeLetter ( int indexI, int indexJ ) {
            _listLetters[indexI][indexJ].letterCntr.OnDownLetter -= OnDownLetter;
            _listLetters[indexI][indexJ].letterCntr.OnUpLetter -= OnUpLetter;
            _listLetters[indexI][indexJ].letterCntr.OnEnterLetter -= OnEnterLetter;
            _listLetters[indexI][indexJ].letterCntr.OnAddNewLetter -= AddNewLetter;
        }

        internal void InitQuest ( Quests quest ) {
            _quest = quest;

            _sizeI = quest.sizeField;
            _sizeJ = quest.sizeField;
            SetParametersSizeField( quest );

            ListLettersBackToPool();

            _listLetters = new List<List<LettersLevel>>();

            if ( _arrayWords != null ) {
                LetterDonePosBackToPool();
            }

            if ( letterDoneButtonsI != null ) {
                LetterDoneButtonIBackToPool();
            }

            for ( int i = 0; i < _sizeI; i++ ) {
                _listLetters.Add( new List<LettersLevel>() );
                for ( int j = 0; j < _sizeJ; j++ ) {
                    LettersLevel lettersLevel = new LettersLevel();
                    lettersLevel.isEmpty = true;
                    _listLetters[i].Add( new LettersLevel() );
                }
            }

            _lettersTask = new List<string>();
            _checkLetters = new List<ArrayIndexMatrinx>();
            _arrayWords = new List<List<LetterDone>>();
            _arrayWordsBool = new List<List<bool>>();
            letterDoneButtonsI = new List<LetterDoneButtonI>();

            int lenghtWord = quest.words[LocalizationManager.GetLanguageString()][0].Length;
            for ( int i = 0; i < quest.words[LocalizationManager.GetLanguageString()].Count; i++ ) {
                if ( quest.words[LocalizationManager.GetLanguageString()][i].Length > lenghtWord ) {

                    lenghtWord = quest.words[LocalizationManager.GetLanguageString()][i].Length;
                }
            }
            _mostLenghtWord = lenghtWord;

            InitLettersFiled( quest );

            if ( nameLevel.Equals( "" ) ) {
                nameLevel = quest.nameLevel;
                InitTasks();
            }
            else if ( !nameLevel.Equals( quest.nameLevel ) ) {
                nameLevel = quest.nameLevel;
            }

            if ( _canvasGroup.alpha <= 0 ) {
                _canvasGroup.DOFade( 1, 0.6f );
            }

            if ( _winUI.IsShown ) {
                _winUI.HideWin();
            }

            _win = false;
            StartPuzzleUpdate();
            OnStartQuest( _quest );
        }

        void InitLettersFiled ( Quests quest ) {
            List<string> words = quest.words[LocalizationManager.GetLanguageString()];
            for ( int i = 0; i < words.Count; i++ ) {

                _lettersTask.Add( "" );
                int numLetter = 0;
                _checkLetters.Add( new ArrayIndexMatrinx() );
                _arrayWords.Add( new List<LetterDone>() );
                _arrayWordsBool.Add( new List<bool>() );

                float startPosXLetterDone = 0 - ( ( parametersLettersDone.stepPosLetter * ( words[i].Length - 1 ) ) / 2f );

                for ( int j = 0; j < words[i].Length; j++ ) {

                    int indexI = quest.indexLetters[i].indexLetters[j].indI;
                    int indexJ = quest.indexLetters[i].indexLetters[j].indJ;

                    CreateLetterAndLatterFly( indexI, indexJ, words[i][j].ToString().ToUpper(), i );

                    numLetter++;

                    _lettersTask[_lettersTask.Count - 1] += words[i][j].ToString().ToUpper();

                    _listLetters[indexI][indexJ].indexWord = i;
                    _listLetters[indexI][indexJ].indexLetter = j;
                    _listLetters[indexI][indexJ].isEmpty = false;
                    _checkLetters[_checkLetters.Count - 1].AddNewLetterIndex( new IndexMatrinx( indexI, indexJ ) );

                    LetterDone goPos = CreatLetterDone( i, j, words[i][j].ToString().ToUpper(), startPosXLetterDone );

                    _arrayWords[i].Add( goPos );
                    _arrayWordsBool[i].Add( quest.indexLetters[i].indexLetters[j].isHint );

                    if ( quest.indexLetters[i].indexLetters[j].isHint ) {
                        goPos.SetEnabledImage( true );
                        goPos.SetEnabledText( true );
                    }
                }

                if ( true ) {
                    LetterDoneButtonI letterDoneButtonI = CreateLetterDoneButtonI( i, words[i].Length, startPosXLetterDone );
                    letterDoneButtonI.OnClickButtinI += ShowHintWordWindow;
                    letterDoneButtonsI.Add( letterDoneButtonI );
                }
            }
        }

        public void SetLettersTaskByIndex ( int indexLettersTask, string word ) {
            if ( indexLettersTask < 0 || indexLettersTask >= _lettersTask.Count ) {
                return;
            }

            _lettersTask[indexLettersTask] = word;
        }

        void SetParametersSizeField ( Quests quest ) {
            float startPosY = 833f;

            //min, max
            Vector2 percentRatioFieldAndDone = new Vector2( 25f, 40f );
            float fullSizeGameFieldY = 1680f;

            //---------------------------
            float startSizeYDone = 120f;
            float indentDone = 10f;

            int wordsCount = quest.words[LocalizationManager.GetLanguageString()].Count;
            float entryFullSizeDone = ( startSizeYDone + indentDone ) * wordsCount;

            float percentDoneTemp = ( entryFullSizeDone / fullSizeGameFieldY ) * 100f;

            if ( percentDoneTemp < percentRatioFieldAndDone.x ) {
                percentDoneTemp = percentRatioFieldAndDone.x;
            }
            else if ( percentDoneTemp > percentRatioFieldAndDone.y ) {
                percentDoneTemp = percentRatioFieldAndDone.y;
            }

            float fullSizeYDone = ( percentDoneTemp * fullSizeGameFieldY ) / 100f;
            float scaleFactorDoneTemp = fullSizeYDone / entryFullSizeDone;

            if ( scaleFactorDoneTemp > 1 ) {
                scaleFactorDoneTemp = 1f;
            }

            float stepPosLetterDoneTemp = ( startSizeYDone + indentDone ) * scaleFactorDoneTemp;
            float middleDonePosY = startPosY - ( fullSizeYDone / 2 );

            float middleDonePosY2 = middleDonePosY + ( ( stepPosLetterDoneTemp * ( wordsCount - 1 ) ) / 2f );

            parametersLettersDone.stepPosLetter = stepPosLetterDoneTemp;
            parametersLettersDone.startPosLetter = new Vector2( 0f, middleDonePosY2 );
            parametersLettersDone.scaleFactor = scaleFactorDoneTemp;

            backLetterDone.sizeDelta = new Vector2( backLetterDone.sizeDelta.x, fullSizeYDone + 60f );
            //--------------------------------------
            float diffPersentField = 100f - percentDoneTemp;
            float fullSizeField = ( diffPersentField * fullSizeGameFieldY ) / 100f;

            float sizeLetter = 530f;
            float indentField = 58f;

            float entryFullSizeField = ( sizeLetter + indentField ) * quest.sizeField;

            float scaleFactorFieldTemp = fullSizeField / entryFullSizeField;
            if ( scaleFactorFieldTemp > 1f ) {
                scaleFactorFieldTemp = 1f;
            }

            float stepPosLetterTemp = ( sizeLetter + indentField ) * scaleFactorFieldTemp;

            float middleFieldPosY = startPosY - fullSizeYDone - ( fullSizeField / 2 );

            float middleFieldPosY2 = middleFieldPosY + ( ( stepPosLetterTemp * ( quest.sizeField - 1 ) ) / 2f );
            float middleFieldPosX2 = 0 - ( ( stepPosLetterTemp * ( quest.sizeField - 1 ) ) / 2f );

            Vector2 startPosField = new Vector2( middleFieldPosX2, middleFieldPosY2 );

            parametersLettersField.startPosLetter = startPosField;
            parametersLettersField.stepPosLetter  = stepPosLetterTemp;
            parametersLettersField.scaleFactor = scaleFactorFieldTemp;
        }

        void CreateLetterAndLatterFly ( int indexI, int indexJ, string newLetter, int numWord ) {

            newLetter = newLetter.ToUpper();
            _listLetters[indexI][indexJ].letter = newLetter;

            LetterController goLetterUI = _lettersPool.PullLetterController();
            goLetterUI.gameObject.name = "letter_" + indexI.ToString() + "_" + indexJ.ToString();
            goLetterUI.transform.SetParent( _letterUIField, false );
            Vector2 letterPosition = new Vector2( parametersLettersField.startPosLetter.x + parametersLettersField.stepPosLetter * indexJ,
                                                  parametersLettersField.startPosLetter.y - parametersLettersField.stepPosLetter * indexI );
            goLetterUI.SetPosition( letterPosition );
            goLetterUI.transform.localScale = new Vector3( parametersLettersField.scaleFactor, parametersLettersField.scaleFactor, 1f );

            _listLetters[indexI][indexJ].letterCntr = goLetterUI;
            // переписать
            _listLetters[indexI][indexJ].letterCntr.SetSrart( indexI, indexJ, _listLetters[indexI][indexJ].letter );
            SubscribeLetter( indexI, indexJ );
            //----------

            LetterDoneFly goLetterDoneUI = _lettersPool.PullLetterDone();
            goLetterDoneUI.gameObject.name = "letter_done_fly_" + indexI.ToString() + "_" + indexJ.ToString();
            goLetterDoneUI.transform.SetParent( _letterDoneUIContanerFly, false );
            goLetterDoneUI.transform.localScale = new Vector3( parametersLettersDone.scaleFactor, parametersLettersDone.scaleFactor, 1f );
            goLetterDoneUI.SetStart( _listLetters[indexI][indexJ].letter, letterPosition );

            _listLetters[indexI][indexJ].letterDone = goLetterDoneUI;
        }

        LetterDone CreatLetterDone ( int numWord, int numLetter, string letter, float startPosXLetterDone ) {

            letter = letter.ToUpper();
            LetterDone goLetterUI = _lettersPool.PullLetterDonePos();
            goLetterUI.name = "letter_done_" + numWord.ToString() + "_" + numLetter.ToString();
            goLetterUI.transform.SetParent( _letterDoneUIContaner, false );
            goLetterUI.transform.localScale = new Vector2( parametersLettersDone.scaleFactor, parametersLettersDone.scaleFactor );
            goLetterUI.SetPosition( new Vector2( startPosXLetterDone + parametersLettersDone.stepPosLetter * numLetter,
                                                 parametersLettersDone.startPosLetter.y - parametersLettersDone.stepPosLetter * numWord ) );
            goLetterUI.SetEnabledImage( false );
            goLetterUI.SetEnabledText( false );
            goLetterUI.SetLetter( letter );

            return goLetterUI;
        }

        LetterDoneButtonI CreateLetterDoneButtonI ( int numWord,  int sizeWord, float startPosXLetterDone ) {
            LetterDoneButtonI letterDoneButtonI = _lettersPool.PullLetterDoneButtonI();
            letterDoneButtonI.name = "letter_done_bitton_ui_" + numWord;
            letterDoneButtonI.transform.SetParent( _letterDoneUIContaner, false );
            letterDoneButtonI.SetPosition( new Vector2( startPosXLetterDone + parametersLettersDone.stepPosLetter * sizeWord,
                                           parametersLettersDone.startPosLetter.y - parametersLettersDone.stepPosLetter * numWord ) );
            letterDoneButtonI.SetStart( numWord );

            return letterDoneButtonI;
        }

        void StartShowLetters () {

            bool isPlayAudio = false;
            for ( int i = 0; i < _lettersTask.Count; i++ ) {
                if ( string.Compare( _stringTask.ToLower(), _lettersTask[i].ToLower() ) == 0 && _stringTask != "" &&
                        !_doneTask[i] ) {

                    _doneTask[i] = true;
                    RecognizeWord( i );
                    break;
                }
            }

            OnWrongWord( _stringTask );

            _stringTask = "";
            _inputText.text = "";
            trailSystem.EndTrial();

            _isMouseDown = false;

            _downLetters = new List<IndexMatrinx>();

            int indexShow = 0;
            for ( int i = 0; i < _sizeI; i++ ) {

                for ( int j = 0; j < _sizeJ; j++ ) {

                    if ( !_listLetters[i][j].isEmpty ) {

                        if ( !_listLetters[i][j].done ) {
                            if ( _listLetters[i][j].tempDone ) {

                                isPlayAudio = true;
                            }
                            _listLetters[i][j].letterCntr.Activate();
                            _listLetters[i][j].tempDone = false;
                            _listLetters[i][j].letterCntr.StartShow( indexShow );
                            indexShow++;
                        }
                    }
                }
            }

            for ( int i = hideLettes.Count - 1; i >= 0; i-- ) {
                if ( !hideLettes[i].isEmpty ) {
                    if ( !hideLettes[i].done ) {
                        if ( hideLettes[i].tempDone ) {
                            isPlayAudio = true;
                        }

                        hideLettes[i].letterCntr.Activate();
                        hideLettes[i].tempDone = false;
                        hideLettes[i].letterCntr.StartShow( i );
                    }
                }
            }

            if ( isPlayAudio ) {

                _loseCount++;

                if ( _loseCount >= _GC.countLooseToShowInfo[_GC.currCountShow] ) {

                    _loseCount = 0;
                    _GC.currCountShow++;

                    if ( _GC.currCountShow >= _GC.countLooseToShowInfo.Count ) {

                        _GC.currCountShow = _GC.countLooseToShowInfo.Count - 1;
                    }
                }

                string nameaud = _loseAudio[UnityEngine.Random.Range( 0, _loseAudio.Length )];
                _myAudioSource.clip = Resources.Load<AudioClip>( "Common_sounds/" + nameaud );
                _myAudioSource.Play();
            }

            hideLettes = new List<LettersLevel>();
        }

        private void OnUpLetter ( int indexI, int indexJ ) {
            if ( !_isActivePuzzle ) {
                return;
            }
            if ( !_isMouseDown ) {
                return;
            }

            _isMouseDown = false;
            StartShowLetters();
        }

        private void OnDownLetter( int indexI, int indexJ ) {
            if ( !_isActivePuzzle ) {
                return;
            }
            //a
            _listLetters[indexI][indexJ].letterCntr.StartHide();
            //b, b должно идти строко после а, иначе буква записывается дважды
            _isMouseDown = true;
            OnStartHideLetter( indexI, indexJ );

            hideLettes.Add( _listLetters[indexI][indexJ] );
        }

        void OnEnterLetter( int indexI, int indexJ ) {
            if ( !_isActivePuzzle ) {
                return;
            }

            if ( !_isMouseDown ) {
                return;
            }

            _listLetters[indexI][indexJ].letterCntr.StartHide();
            OnStartHideLetter( indexI, indexJ );

            hideLettes.Add( _listLetters[indexI][indexJ] );
        }

        public void AddNewLetter ( int indexI, int indexJ ) {

            IndexMatrinx tempIndexMatrinx = new IndexMatrinx();
            tempIndexMatrinx.indI = indexI;
            tempIndexMatrinx.indJ = indexJ;

            if ( _downLetters.Contains( tempIndexMatrinx ) ) {
                return;
            }

            _downLetters.Add( tempIndexMatrinx );

            _stringTask += _listLetters[indexI][indexJ].letter;

            _inputText.text = _stringTask;

            _listLetters[indexI][indexJ].tempDone = true;

            trailSystem.StartTrial();

            for ( int i = 0; i < _sizeI; i++ ) {

                for ( int j = 0; j < _sizeJ; j++ ) {

                    if ( !_listLetters[i][j].isEmpty ) {

                        if ( ( Mathf.Abs( indexI - i ) <= 1 && Mathf.Abs( indexJ - j ) <= 1 ) && !_listLetters[i][j].tempDone ) {

                            _listLetters[i][j].letterCntr.Activate();
                        }
                        else {

                            _listLetters[i][j].letterCntr.DiActivate();
                        }
                    }
                }
            }
        }

        bool IsEndField ( Vector2 newPosition ) {

            return newPosition.x > _plusEdge.x || newPosition.x < _minusEdge.x ||
                   newPosition.y > _plusEdge.y || newPosition.y < _minusEdge.y;
        }

        void RecognizeWord ( int indexTemp ) {

            float timeFly = 0;
            for ( int i = 0; i < _downLetters.Count; i++ ) {

                timeFly = i / 10f;
                _listLetters[_downLetters[i].indI][_downLetters[i].indJ].done = true;

                if ( _arrayWords[indexTemp][i] != null ) {

                    _arrayWordsBool[indexTemp][i] = true;
                    Vector2 posFly = _arrayWords[indexTemp][i].gameObject.GetComponent<RectTransform>().anchoredPosition;
                    _listLetters[_downLetters[i].indI][_downLetters[i].indJ].letterDone.StartFly( posFly, timeFly );
                }
            }

            _myAudioSource.clip = Resources.Load<AudioClip>( "Common_sounds/levelstart" );
            _myAudioSource.Play();

            _downLetters = new List<IndexMatrinx>();
            _stringTask = "";
            _inputText.text = "";

            //lettersTempTask.RemoveAt( indexTemp );

            if ( IsWinTasks() ) {
                OnWinQuest();

                _win = true;
                nameLevel = "";
                StopPuzzleUpdate();

                StartCoroutine( WinWind( timeFly + 0.3f ) );
            }
        }

        bool IsWinTasks () {

            for ( int i = 0; i < _doneTask.Count; i++ ) {
                if ( !_doneTask[i] ) {

                    return false;
                }
            }
            return true;
        }

        IEnumerator WinWind ( float timeWait ) {

            yield return new WaitForSeconds( timeWait );

            for ( int i = 0; i < _quest.words[LocalizationManager.GetLanguageString()].Count; i++ ) {

                for ( int j = 0; j < _quest.words[LocalizationManager.GetLanguageString()][i].Length; j++ ) {

                    _quest.indexLetters[i].indexLetters[j].isHint = false;
                }
            }

            //финально завершение пазла
            //_levelUIGroup.GetComponent<Animator>().SetTrigger( "startHideEnd" );
            _canvasGroup.DOFade( 0f, 0.6f );

            GameObject goSound = GameObject.Find( "sound_object_2" );

            if ( goSound != null ) {
                if ( goSound.GetComponent<AudioSource>() != null ) {

                    goSound.GetComponent<AudioSource>().clip = Resources.Load<AudioClip>( "Common_sounds/startGame" );
                    goSound.GetComponent<AudioSource>().Play();
                }
            }

            ShowWinWindow( PuzzleType.Editor, 0, 0, LevelFinishType.FinLevel, 1 );
        }

        void ShowWinWindow ( PuzzleType puzzelType, int newFromLeve, int newToLevel, LevelFinishType levelFinishType, int newNumPostText ) {
            _winUI.ShowWin( new WinUIParams() {
                puzzelType = puzzelType,
                newFromLeve = newFromLeve,
                newToLevel = newToLevel,
                levelFinishType = levelFinishType,
                newNumPostText = newNumPostText
            } );
        }

        void EndLevel() {

            _canvasGroup.DOFade( 1f, 0.6f ).OnComplete( () => {
                RestartEnd();
            } );
        }

        void InitTasks () {

            _stringTask = "";
            _inputText.text = "";

            _doneTask = new List<bool>();

            if ( _lettersTask == null ) {
                return;
            }

            for ( int i = 0; i < _lettersTask.Count; i++ ) {

                _doneTask.Add( false );
            }
        }

        IEnumerator RestartWait ( float timeWait, Action actinoAfterWait ) {
            if ( !_contHideletters ) {
                timeWait = 0;
            }
            yield return new WaitForSeconds( timeWait );
            if ( _contHideletters ) {
                actinoAfterWait();
            }
        }

        public void RestartEnd () {
            StartPuzzleUpdate();

            _lockUI.Lock( false );
        }

        void ClearField () {

            ListLettersBackToPool();
            LetterDonePosBackToPool();
            _arrayWords = new List<List<LetterDone>>();

            _listLetters = new List<List<LettersLevel>>();

            StopPuzzleUpdate();

            if ( _winUI.IsShown ) {
                _winUI.HideWin();
            }
        }

        void ListLettersBackToPool () {
            for ( int i = 0; i < _listLetters.Count; i++ ) {
                for ( int j = 0; j < _listLetters[i].Count; j++ ) {
                    if ( _listLetters[i][j].letterCntr != null ) {
                        UnSubscribeLetter( i, j );

                        _lettersPool.PushLetterController( _listLetters[i][j].letterCntr );
                        _listLetters[i][j].letterCntr = null;
                    }

                    if ( _listLetters[i][j].letterDone != null ) {
                        _lettersPool.PushLetterDone( _listLetters[i][j].letterDone );
                        _listLetters[i][j].letterDone = null;
                    }
                }
                _listLetters[i].Clear();
            }
        }

        void LetterDonePosBackToPool () {
            for ( int i = 0; i < _arrayWords.Count; i++ ) {
                for ( int j = 0; j < _arrayWords[i].Count; j++ ) {
                    _lettersPool.PushLetterDonePos( _arrayWords[i][j] );
                }
            }
            _arrayWords = new List<List<LetterDone>>();
        }

        void LetterDoneButtonIBackToPool () {
            for ( int i = 0; i < letterDoneButtonsI.Count; i++ ) {
                letterDoneButtonsI[i].OnClickButtinI -= ShowHintWordWindow;
                _lettersPool.PushLetterDoneButtonI( letterDoneButtonsI[i] );
            }

            letterDoneButtonsI = new List<LetterDoneButtonI>();
        }

        void ShowHintWordWindow ( int indexWord ) {
        }

        internal void ClickButtonBack () {
            ClearField();

            OnClickButtonBack();
        }

        internal void SetPuzzelType ( PuzzleType puzzelType ) {
            _puzzelType = puzzelType;
        }
    }
}

