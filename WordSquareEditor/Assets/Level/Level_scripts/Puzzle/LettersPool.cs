﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class LettersPool: MonoBehaviour {
        [SerializeField]
        LetterController _letterControllerPrf;
        [SerializeField]
        RectTransform _letterControllerParent;
        int _sizeLetterPool = 90;

        [SerializeField]
        LetterDoneFly _letterDonePrf;
        [SerializeField]
        RectTransform _letterDoneParent;
        int _sizeLetterDonePool = 30;

        [SerializeField]
        LetterDone _letterDonePosPrf;
        [SerializeField]
        RectTransform _letterDonePosParent;
        int _sizeLetterDonePosPool = 30;

        [SerializeField]
        LetterDoneButtonI letterDoneButtonIPrf;
        [SerializeField]
        RectTransform letterDoneButtonIParent;
        int sizeletterDoneButtonI = 20;

        void Awake () {
            InitLetterPool();
            InitLetterDonePool();
            InitLetterDonePosPool();
            InitletterDoneButtonI();
        }

        void InitLetterPool () {
            for ( int i = 0; i < _sizeLetterPool; i++ ) {
                CreateLetter();
            }
        }

        void InitLetterDonePool () {
            for ( int i = 0; i < _sizeLetterDonePool; i++ ) {
                CreateLetterDone();
            }
        }

        void InitLetterDonePosPool () {
            for ( int i = 0; i < _sizeLetterDonePosPool; i++ ) {
                CreateLetterDonePos();
            }
        }

        void InitletterDoneButtonI () {
            for ( int i = 0; i < sizeletterDoneButtonI; i++ ) {

            }
        }

        LetterController CreateLetter () {
            LetterController goLetterUI = Instantiate( _letterControllerPrf, Vector2.zero, Quaternion.identity );
            goLetterUI.transform.SetParent( _letterControllerParent, false );
            goLetterUI.gameObject.SetActive( false );
            return goLetterUI;
        }

        internal LetterController PullLetterController () {
            LetterController letter;

            if ( _letterControllerParent.childCount <= 0 ) {
                letter = CreateLetter ();
                letter.gameObject.SetActive( true );
                return letter;
            }

            GameObject objectLetter = null;
            for ( int i = 0; i < _letterControllerParent.childCount; i++ ) {
                if ( _letterControllerParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectLetter = _letterControllerParent.GetChild( i ).gameObject;
            }

            if ( objectLetter == null ) {
                letter = CreateLetter();
                letter.gameObject.SetActive( true );
                return letter;

            }
            letter = objectLetter.GetComponent<LetterController>();
            letter.gameObject.SetActive( true );
            return letter;
        }

        internal void PushLetterController ( LetterController goLetterUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( _letterControllerParent.childCount + 1 > _sizeLetterPool ) {
                Destroy( goLetterUI.gameObject );
                return;
            }

            goLetterUI.transform.SetParent( _letterControllerParent, false );
            goLetterUI.gameObject.SetActive( false );
        }

        LetterDoneFly CreateLetterDone () {
            LetterDoneFly goLetterDoneUI = Instantiate( _letterDonePrf, Vector2.zero, Quaternion.identity );
            goLetterDoneUI.transform.SetParent( _letterDoneParent, false );
            goLetterDoneUI.gameObject.SetActive( false );
            return goLetterDoneUI;
        }

        internal LetterDoneFly PullLetterDone () {
            LetterDoneFly letterDone;

            if ( _letterDoneParent.childCount <= 0 ) {
                letterDone = CreateLetterDone();
                letterDone.gameObject.SetActive( true );
                return letterDone;
            }

            GameObject objectLetter = null;
            for ( int i = 0; i < _letterDoneParent.childCount; i++ ) {
                if ( _letterDoneParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                objectLetter = _letterDoneParent.GetChild( i ).gameObject;
            }

            if ( objectLetter == null ) {
                letterDone = CreateLetterDone();
                letterDone.gameObject.SetActive( true );
                return letterDone;

            }
            letterDone = objectLetter.GetComponent<LetterDoneFly>();
            letterDone.gameObject.SetActive( true );
            return letterDone;
        }

        internal void PushLetterDone ( LetterDoneFly goLetterDoneUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( _letterDoneParent.childCount + 1 > _sizeLetterDonePool ) {
                Destroy( goLetterDoneUI.gameObject );
                return;
            }
            goLetterDoneUI.transform.SetParent( _letterDoneParent, false );
            goLetterDoneUI.gameObject.SetActive( false );
        }

        LetterDone CreateLetterDonePos () {
            LetterDone goLetterDoneUI = Instantiate( _letterDonePosPrf, Vector2.zero, Quaternion.identity );
            goLetterDoneUI.transform.SetParent( _letterDonePosParent, false );
            goLetterDoneUI.gameObject.SetActive( false );
            return goLetterDoneUI;
        }

        internal LetterDone PullLetterDonePos () {
            LetterDone letterDonePos = null;

            if ( _letterDonePosParent.childCount <= 0 ) {
                letterDonePos = CreateLetterDonePos();
                letterDonePos.gameObject.SetActive( true );
                return letterDonePos;
            }

            GameObject letterDonePosGo = null;
            for ( int i = 0; i < _letterDonePosParent.childCount; i++ ) {
                if ( _letterDonePosParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                letterDonePosGo = _letterDonePosParent.GetChild( i ).gameObject;
            }

            if ( letterDonePosGo == null ) {
                letterDonePos = CreateLetterDonePos();
                letterDonePos.gameObject.SetActive( true );
                return letterDonePos;

            }

            letterDonePos = letterDonePosGo.GetComponent<LetterDone>();
            letterDonePos.gameObject.SetActive( true );
            return letterDonePos;
        }

        internal void PushLetterDonePos ( LetterDone goLetterDonePosUI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( _letterDonePosParent.childCount + 1 > _sizeLetterDonePool ) {

                Destroy( goLetterDonePosUI.gameObject );
                return;
            }

            goLetterDonePosUI.transform.SetParent( _letterDonePosParent, false );
            goLetterDonePosUI.gameObject.SetActive( false );
        }

        LetterDoneButtonI CreateLetterDoneButtonI () {
            LetterDoneButtonI goLetterDoneButtonIUI = Instantiate( letterDoneButtonIPrf, Vector2.zero, Quaternion.identity );
            goLetterDoneButtonIUI.transform.SetParent( letterDoneButtonIParent, false );
            goLetterDoneButtonIUI.gameObject.SetActive( false );
            return goLetterDoneButtonIUI;
        }

        internal LetterDoneButtonI PullLetterDoneButtonI () {
            LetterDoneButtonI letterDoneButtonI = null;

            if ( letterDoneButtonIParent.childCount <= 0 ) {
                letterDoneButtonI = CreateLetterDoneButtonI();
                letterDoneButtonI.gameObject.SetActive( true );
                return letterDoneButtonI;
            }

            GameObject letterDoneButtonIGo = null;
            for ( int i = 0; i < letterDoneButtonIParent.childCount; i++ ) {
                if ( letterDoneButtonIParent.GetChild( i ).gameObject.activeSelf ) {
                    continue;
                }
                letterDoneButtonIGo = letterDoneButtonIParent.GetChild( i ).gameObject;
            }

            if ( letterDoneButtonIGo == null ) {
                letterDoneButtonI = CreateLetterDoneButtonI();
                letterDoneButtonI.gameObject.SetActive( true );
                return letterDoneButtonI;

            }

            letterDoneButtonI = letterDoneButtonIGo.GetComponent<LetterDoneButtonI>();
            letterDoneButtonI.gameObject.SetActive( true );
            return letterDoneButtonI;
        }

        internal void PushLetterDoneButtonI ( LetterDoneButtonI goLetterDoneButtonI ) {
            //if ( goLetterUI == null ) {
            //    return;
            //}

            if ( letterDoneButtonIParent.childCount + 1 > sizeletterDoneButtonI ) {

                Destroy( goLetterDoneButtonI.gameObject );
                return;
            }

            goLetterDoneButtonI.transform.SetParent( letterDoneButtonIParent, false );
            goLetterDoneButtonI.gameObject.SetActive( false );
        }
    }
}

