﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public interface IPuzzleDifference {
        void Init ( PuzzleController puzzleController );
        void PerfomPuzzleEnd();
    }
}
