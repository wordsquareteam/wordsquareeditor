﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct FieldLettersParameters {
    public Vector2 startPosLetter;
    public float stepPosLetter;
    public float scaleFactor;
}
