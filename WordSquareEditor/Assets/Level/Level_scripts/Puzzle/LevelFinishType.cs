﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public enum PuzzleType {
        Common,
        Daily,
        Editor
    }

    public enum LevelFinishType {
        HintAdded1,
        HintAdded2,
        GoToMenu,
        FinLevel,
        None
    }
}

