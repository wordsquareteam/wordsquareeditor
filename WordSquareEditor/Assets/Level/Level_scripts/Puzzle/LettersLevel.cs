﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class LettersLevel {
        public string letter;
        public bool tempDone;
        public bool done;
        internal LetterController letterCntr;
        public LetterDoneFly letterDone;

        public int indexWord;
        public int indexLetter;

        public bool isEmpty;

        public LettersLevel () {

            tempDone = false;
            done = false;
            isEmpty = true;
        }
    }
}
