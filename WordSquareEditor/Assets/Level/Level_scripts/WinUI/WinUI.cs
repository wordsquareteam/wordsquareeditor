﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using Common;

namespace Puzzle {
    public class WinUI: WindowMonoBehaviour {
        public event Action<PuzzleType, int, int> OnStartShow = ( PuzzleType param1, int param2, int param3 ) => { };
        public event Action<PuzzleType, LevelFinishType> OnStartFinLevel = ( PuzzleType param1, LevelFinishType  param2 ) => { };

        [SerializeField]
        Text title;
        [SerializeField]
        Text numLevel;
        [SerializeField]
        Image backLevel;
        [SerializeField]
        Text postText;
        [SerializeField]
        GameObject psAnim;
        [SerializeField]
        AudioSource myAudioSource;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        Button _winClick;
        [SerializeField]
        Text _categoriesTittle;
        [SerializeField]
        GameObject _hintPanel;
        [SerializeField]
        Text _hintCount;
        Coroutine _startChangLevel;
        Coroutine _startFinLevel;

        bool _endStateFirst = false;
        bool _endStateSecond = false;

        PuzzleType _puzzelType;

        string[] cangritWords;
        string[] postTextString;
        int fromLeve;
        int toLevel;

        LevelFinishType _levelFinishType;

        public void Awake () {
            _winClick.onClick.AddListener( SetEndStartWinWindow );
            _myAnimator.enabled = false;
        }

        private void Start () {
            cangritWords = new string[] { "txt_puzzle_win_tittle_1", "txt_puzzle_win_tittle_2", "txt_puzzle_win_tittle_3", "txt_puzzle_win_tittle_4" };
            postTextString = new string[] { "txt_puzzle_win_tittle_new_level", "txt_puzzle_win_tittle_level_already_solved", "txt_puzzle_win_tittle_bonus_hint_added" };
        }

        public void ShowWin ( WinUIParams winUIParams ) {
            _endStateFirst = false;
            _endStateSecond = false;

            _puzzelType = winUIParams.puzzelType;

            int rnadomNum = UnityEngine.Random.Range( 0, cangritWords.Length );
            title.text = LocalizationManager.GetText( cangritWords[rnadomNum] );

            _levelFinishType = winUIParams.levelFinishType;

            fromLeve = winUIParams.newFromLeve;
            toLevel = winUIParams.newToLevel;

            psAnim.gameObject.SetActive( false );

            numLevel.text = fromLeve.ToString();
            backLevel.color = new Color( UnityEngine.Random.Range( 0.5f, 1f ), UnityEngine.Random.Range( 0.5f, 1f ), UnityEngine.Random.Range( 0.5f,
                                         1f ) );

            postText.text = LocalizationManager.GetText( postTextString[winUIParams.newNumPostText] );

            base.Show();

            myAudioSource.Play();

            if ( winUIParams.puzzelType.Equals( PuzzleType.Common ) && winUIParams.levelFinishType.Equals( LevelFinishType.GoToMenu ) ) {
                _categoriesTittle.gameObject.SetActive( true );
            }
            else {
                _categoriesTittle.gameObject.SetActive( false );
            }

            if ( winUIParams.levelFinishType.Equals( LevelFinishType.HintAdded1 ) ) {
                _hintPanel.gameObject.SetActive( true );
                _hintCount.text = "1";
            }
            else if ( winUIParams.levelFinishType.Equals( LevelFinishType.HintAdded2 ) ) {
                _hintPanel.gameObject.SetActive( true );
                _hintCount.text = "2";
            }
            else {
                _hintPanel.gameObject.SetActive( false );
            }

            numLevel.gameObject.SetActive( true );

            _myAnimator.enabled = true;
            _myAnimator.Play( "win" );
            Invoke( "StartChangeLevel", 0.33f );
            Invoke( "TryPlayAudio", 0.3f );

            OnStartShow( winUIParams.puzzelType, winUIParams.newFromLeve, winUIParams.newToLevel );
        }

        void StartChangeLevel () {
            if ( _endStateFirst ) {
                return;
            }

            if ( fromLeve != toLevel ) {

                _startChangLevel = StartCoroutine( StartChangLevel() );
            }
            else {

                _startFinLevel = StartCoroutine( StartFinLevel() );
            }
        }

        IEnumerator StartChangLevel () {
            yield return new WaitForSeconds( 0.3f );
            if ( !_endStateFirst ) {
                numLevel.text = toLevel.ToString();
                psAnim.SetActive( true );

                _startFinLevel = StartCoroutine( StartFinLevel() );
            }
        }

        void TryPlayAudio () {

            GameObject go = GameObject.Find( "sound_object" );
            if ( go != null ) {

                go.GetComponent<AudioSource>().Play();
            }
        }

        IEnumerator StartFinLevel () {
            yield return new WaitForSeconds( 3.0f );

            if ( !_endStateSecond ) {
                StartHide();
            }
        }

        void StartHide () {
            if ( IsShown ) {
                HideWin();
            }

            OnStartFinLevel( _puzzelType, _levelFinishType );
        }

        internal void HideWin () {
            _myAnimator.Play( "hide" );
            Invoke( "HideEnd", 0.3f );
        }

        void SetEndStartWinWindow () {
            if ( !_endStateFirst ) {
                _endStateFirst = true;

                numLevel.text = toLevel.ToString();

                if ( _startChangLevel != null ) {
                    StopCoroutine( _startChangLevel );
                }

                _startFinLevel = StartCoroutine( StartFinLevel() );

                return;
            }
            else if ( !_endStateSecond ) {
                _endStateSecond = true;

                if ( _startFinLevel != null ) {
                    StopCoroutine( _startFinLevel );
                }

                StartHide();
            }
        }

        void HideEnd () {
            base.Hide();

            _myAnimator.enabled = false;
        }

        internal void SetLevelFinishType ( LevelFinishType levelFinishType ) {
            this._levelFinishType = levelFinishType;
        }
    }
}
