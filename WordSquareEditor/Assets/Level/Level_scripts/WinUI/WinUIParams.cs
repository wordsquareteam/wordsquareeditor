﻿using UnityEngine;
using Puzzle;

public struct WinUIParams {
    public PuzzleType puzzelType;
    public int newFromLeve;
    public int newToLevel;
    public LevelFinishType levelFinishType;
    public int newNumPostText;
}
