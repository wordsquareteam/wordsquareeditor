﻿using UnityEngine;
using System;
using UnityEngine.UI;
using Common;
using DG.Tweening;
using System.Collections;
using Utility;

namespace Puzzle {
    internal class LetterController: MonoBehaviour {

        internal event Action<int, int> OnDownLetter = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnUpLetter = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnEnterLetter = ( int indexI, int indexJ ) => { };
        internal event Action<int, int> OnAddNewLetter = ( int indexI, int indexJ ) => { };
        [SerializeField]
        RectTransform _myRectTransform;
        [SerializeField]
        Image backImage;
        [SerializeField]
        Text letterText;
        [SerializeField]
        CircleCollider2D myCollider;
        [SerializeField]
        RectTransform childTransform;

        int id;
        string letter;
        internal string Letter {
            get {
                return letter;
            }
        }

        bool startHide;

        bool scaleHide;
        bool scaleShow;
        int indexI;
        int indexJ;
        AudioSource myAudioSourse;
        string[] audioFiles;
        internal RectTransform MyRectTransform {
            get {
                return _myRectTransform;
            }
        }

        bool isInit = false;
        ColorDataSet colorDataSet;

        float endScale;
        int countScale;
        Vector3 startPosition;
        bool scaleShowUp;
        bool scaleShowDown;
        bool isShow = true;
        float delayShow = 0;
        bool endHideShow = false;

        internal void Init () {
            if ( isInit ) {
                return;
            }

            isInit = true;
            colorDataSet = GameObject.FindObjectOfType<GameLinks>().ColorDataSet;
        }

        internal void SetSrart ( int newIndexI, int newIndexJ, string newLetter ) {
            Init();

            audioFiles = new string[9] { "tapswipe1", "tapswipe2", "tapswipe3", "tapswipe4", "tapswipe5", "tapswipe6", "tapswipe7", "tapswipe8", "tapswipe9" };
            myAudioSourse = this.gameObject.GetComponent<AudioSource>();

            indexI = newIndexI;
            indexJ = newIndexJ;

            letter = newLetter;

            letterText.text = letter;
            backImage.color = colorDataSet.GetRandomColor();

            RestartShow();
        }

        void FixedUpdate () {
            if ( scaleHide ) {

                if ( childTransform.localScale.x <= 0 ) {
                    scaleHide = false;
                    childTransform.localScale = new Vector2( 0f, 0f );
                    endHideShow = true;
                }
                else {
                    childTransform.localScale -= new Vector3( 0.1f, 0.1f );
                }
            }

            if ( scaleShow ) {
                if ( delayShow > 0 ) {
                    delayShow--;
                    return;
                }

                if ( scaleShowUp ) {
                    if ( childTransform.localScale.x >= endScale ) {
                        if ( countScale < 0 ) {
                            scaleShow = false;
                            childTransform.localScale = new Vector2( 1f, 1f );

                            //childTransform.DOShakeRotation( 0.5f, new Vector3( 0, 0, 20 ) );
                        }
                        else {
                            countScale--;
                            endScale = 1f - ( countScale * countScale / 100f );
                            scaleShowUp = false;
                            //endScale = UnityEngine.Random.Range( 1.2f, 1.4f );
                            if ( endScale < 0 ) {
                                endScale = 0;
                            }
                        }
                    }
                    else {
                        childTransform.localScale += new Vector3( 0.1f, 0.1f );
                    }
                }
                else {
                    if ( childTransform.localScale.x <= endScale ) {
                        if ( countScale < 0 ) {
                            scaleShow = false;
                            childTransform.localScale = new Vector2( 1f, 1f );

                            //childTransform.DOShakeRotation( 0.5f, new Vector3( 0, 0, 20 ) );
                        }
                        else {
                            countScale--;
                            endScale = 1f + ( countScale * countScale / 100f );

                            scaleShowUp = true;
                            //endScale = UnityEngine.Random.Range( 1.2f, 1.4f );
                        }
                    }
                    else {
                        childTransform.localScale -= new Vector3( 0.1f, 0.1f );
                    }
                }
            }
        }

        void OnMouseUp () {
            OnUpLetter( indexI, indexJ );
        }

        void OnMouseEnter () {
            OnEnterLetter( indexI, indexJ );
        }

        internal void StartHide ( bool isGamplay = true ) {
            endHideShow = false;

            myCollider.enabled = false;

            scaleShow = false;
            scaleHide = true;

            isShow = false;

            //childTransform.DOKill();

            //childTransform.DOScale( 0f, 0.2f );
            //startHide = true;

            if ( isGamplay ) {

                string nameaud = audioFiles[UnityEngine.Random.Range( 0, audioFiles.Length )];
                myAudioSourse.clip = Resources.Load<AudioClip>( "Common_sounds/" + nameaud );
                myAudioSourse.Play();

                OnAddNewLetter( indexI, indexJ );
            }
        }

        Coroutine crt;
        internal void StartShow ( int indexShow ) {
            if ( isShow ) {
                return;
            }
            //childTransform.DOKill();

            //childTransform.DOScale( 1f, 0.2f ).OnComplete( () => {
            //    childTransform.DOShakeScale( 0.3f, 0.5f, 10, 0 );
            //} );
            if ( endHideShow ) {
                delayShow = indexShow;
            }
            else {
                delayShow = 0;
            }

            //countScale = UnityEngine.Random.Range( 3, 6 );
            countScale = 4;
            endScale = 1f + ( countScale * countScale / 100f );

            startPosition = new Vector3( 1f, 1f, 1f );

            startHide = false;

            scaleShow = true;
            scaleHide = false;
            myCollider.enabled = true;

            scaleShowUp = true;
            //scaleShowDown;

            if ( crt != null ) {
                StopCoroutine( crt );
            }
            //crt = StartCoroutine( Oscillation() );

            isShow = true;
        }

        void Show () {


        }

        Vector3 GetRandomScale () {
            if ( endScale > 1 ) {

            }
            else {

            }

            return new Vector3( 0f, 0f, 0f );
            //endScale = UnityEngine.Random.Range( 1.2f, 1.4f );
        }

        internal void RestartShow () {
            scaleShow = false;
            scaleHide = false;
            myCollider.enabled = true;
            childTransform.localScale = new Vector3( 1f, 1f );
        }

        internal void Activate () {
            myCollider.enabled = true;
        }

        internal void DiActivate () {
            myCollider.enabled = false;
        }

        internal void StartHideRestart () {
            StartHide( false );
        }

        internal void OnDownA () {
            OnDownLetter( indexI, indexJ );
        }

        internal void OnUpA () {
            OnUpLetter( indexI, indexJ );
        }

        internal void SetPosition ( Vector2 letterPosition ) {
            _myRectTransform.anchoredPosition = letterPosition;
        }

        #region затухающие колебания
        //Амплитуда. Модуль максимального отклонения тела от положения равновесия. Чем больше амплитуда, тем сильнее будет изменятся переменная.
        [SerializeField]
        float amplitude = 0.9f;
        //Коэффициент затухания. Чем больше коэффициент, тем быстрее затухнут колебания.
        [SerializeField]
        float decay = 1;
        //Частота колебания. Чем больше частота колебания, тем чаще колеблется, кто бы мог подумать только. На графике частота равна трём (3).
        [SerializeField]
        float frequence =  1f;

        //Собственно, сама переменная, которую мы будем менять. Просто приравниваете то, что хотите колебать, к этой переменной.
        float oscillatingVar;
        //Минимальная допустимая разница между двумя кадрами. Это первый параметр (ниже второй), который не даёт циклу быть бесконечным. Не рекомендуется ставить ноль.
        [SerializeField]
        float minVelocity = 0.000001f;
        //Максимальное количество регистрации разницы меньше minVelocity. Чем больше число, тем меньше шанс, что колебания остановятся до значительного затухания, например, на пиках колебаний (самая верхняя граница по модулю).
        int maxMVTimes;
        //Счетчик регистраций разницы меньше minVelocity
        int mvt;

        IEnumerator Oscillation () {
            //Один раз вызываем функцию. Не надо её в апдейт пихать.

            //Регистрируем время вызова функции.
            float startTime = Time.time;
            //Переменная для оси абсцисс.
            float t;
            //Бесконечный цикл, разрыв внутри.
            for ( ; ; ) {
                //Ждём. Иначе не работает ничего.
                yield return new WaitForEndOfFrame();
                //Переменная для расчета разницы между значениями в двух соседних кадрах.
                var z = oscillatingVar;
                //Измеряем время между "вот прямо сейчас" и началом функции.
                t = Time.time - startTime;
                oscillatingVar = amplitude * Mathf.Sin( t * frequence * Mathf.PI * 2 ) / Mathf.Exp( t *
                                 decay );     //Самая главная часть скрипта. Сама функция, по которой и построен график.

                childTransform.localScale = new Vector3( startPosition.x + oscillatingVar, startPosition.y + oscillatingVar, 0 );
                //Debug.Log( Mathf.Abs( oscillatingVar - z ) );                              //Тут можно посмотреть за значениями переменной. Уберите это.

                //Тут, в зависимости от количества регистраций разницы между двумя кадрами меньше minVelocity, разрывается цикл.
                if ( Mathf.Abs( oscillatingVar - z ) < minVelocity &&  mvt > maxMVTimes ) {
                    //isImpulse = false;
                    break;
                }
                else {
                    if ( Mathf.Abs( oscillatingVar - z ) < minVelocity ) {
                        mvt++;
                    }
                }
            }
        }
        #endregion
    }
}
