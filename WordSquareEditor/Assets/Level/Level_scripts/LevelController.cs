﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Menu;
using Common;

namespace Puzzle {
    public class LevelController: MonoBehaviour {

        GameController _GC;

        [SerializeField]
        PuzzleController _puzzleController;

        bool isGoToMenu;

        void Start () {
            _puzzleController.Init();

            isGoToMenu = false;
            _GC = GameObject.FindGameObjectWithTag( "GameController" ).GetComponent<GameController>();

            if ( !_GC.isSound ) {
                Camera.main.GetComponent<AudioListener>().enabled = false;
            }

            _puzzleController.InitQuest( _GC.GetCurrentQuest() );
        }

        void StrartHideLetters () {
        }

        void FinLevel () {
            SceneManager.LoadScene( "level" );
        }


        public void GoToMainMenu () {

            if ( !isGoToMenu ) {

                isGoToMenu = true;
                SceneManager.LoadScene( "Editor_level" );
            }
        }
    }
}