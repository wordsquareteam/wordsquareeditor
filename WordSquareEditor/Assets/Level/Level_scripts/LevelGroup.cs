﻿using UnityEngine;
using System.Collections;

namespace Puzzle {
    public class LevelGroup: MonoBehaviour {

        LevelController _LC;
        PuzzleController _puzzleController;

        void Start () {
            _puzzleController = GameObject.FindObjectOfType<PuzzleController>();

            _puzzleController.Init();
            _puzzleController.SetActivePuzzle( true );
        }

        public void EndHide () {
        }

        public void EndShow () {
        }
    }
}
