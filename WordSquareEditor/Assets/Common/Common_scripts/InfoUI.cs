﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;

namespace Common {
    public class InfoUI: WindowMonoBehaviour {
        [SerializeField]
        Image backImg;
        [SerializeField]
        Text _infoText;
        [SerializeField]
        Animator _myAnimator;
        [SerializeField]
        Button _closeButtom;
        [SerializeField]
        Button _buttonBack;

        string[] stringInfo;
        bool _isStartHide;
        float _hideAnimationLength;
        Coroutine _timeHide;

        void Awake () {
            _closeButtom.onClick.AddListener( StartHide );
            _buttonBack.onClick.AddListener( StartHide );

            _hideAnimationLength = 0f;
            RuntimeAnimatorController _runtimeAnimatorController = _myAnimator.runtimeAnimatorController;
            AnimationClip[] clips = _runtimeAnimatorController.animationClips;

            for ( int i = 0; i < clips.Length; i++ ) {
                if ( clips[i].name.Equals( "hide_info" ) ) {
                    _hideAnimationLength = clips[i].length;
                }
            }

            _myAnimator.enabled = false;

            stringInfo = new string[4] {
                "txt_info_window_complete_other_level_first", "txt_info_window_complete_other_category_first",
                "txt_info_window_already_solve_daily_puzzle", "txt_info_window_already_solve_all_daily_puzzle"
            };
        }

        public void ShowInfo ( int numInfo ) {
            base.Show();

            _myAnimator.enabled = true;

            _infoText.text = LocalizationManager.GetText( stringInfo[numInfo] );
            backImg.color = new Color( Random.Range( 0.5f, 1f ), Random.Range( 0.5f, 1f ), Random.Range( 0.5f, 1f ) );

            _timeHide = StartCoroutine( TimeHide() );

            _isStartHide = false;
            //_myAnimator.Play( "show_info" );
        }

        IEnumerator TimeHide () {
            yield return new WaitForSeconds( 2f );
            StartHide();
        }

        void StartHide () {
            if ( _timeHide != null ) {
                StopCoroutine( _timeHide );
            }

            if ( _isStartHide ) {
                return;
            }

            _isStartHide = true;
            _myAnimator.SetTrigger( "startHide" );

            Invoke( "HideInfo", _hideAnimationLength );
        }

        void HideInfo () {
            base.Hide();
            _myAnimator.enabled = false;
        }
    }
}
