﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Common {
    public struct CatigoriesMainAndDailyQuests {
        public List<CatigoriesQuests> catigoriesQuests;
        public List<string> daylyPuzzleQuest;

        [JsonConstructor]
        public CatigoriesMainAndDailyQuests ( List<CatigoriesQuests> catigoriesQuests, List<string> daylyPuzzleQuest ) {
            this.catigoriesQuests = catigoriesQuests;
            this.daylyPuzzleQuest = daylyPuzzleQuest;
        }
    }
}
