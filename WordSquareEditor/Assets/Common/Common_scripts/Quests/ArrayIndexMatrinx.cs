﻿using System.Collections.Generic;

namespace Common {
    public class ArrayIndexMatrinx {
        public List<IndexMatrinx> indexLetters;

        public ArrayIndexMatrinx () {
            indexLetters = new List<IndexMatrinx>();
        }

        public ArrayIndexMatrinx ( ArrayIndexMatrinx newArrayIndexMatrinx ) {

            indexLetters = new List<IndexMatrinx>();
            for ( int i = 0; i < newArrayIndexMatrinx.indexLetters.Count; i++ ) {

                indexLetters.Add( newArrayIndexMatrinx.indexLetters[i] );
            }
        }
        public void AddNewLetterIndex ( IndexMatrinx newIndexMatrix ) {

            indexLetters.Add( new IndexMatrinx() );
            indexLetters[indexLetters.Count - 1].indI = newIndexMatrix.indI;
            indexLetters[indexLetters.Count - 1].indJ = newIndexMatrix.indJ;
        }
    }
}
