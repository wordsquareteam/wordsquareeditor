﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Common {
    public struct CatigoriesQuests {
        public string nameCatigorie;
        public string imgCatigorie;
        public List<string> nameQuest;

        [JsonConstructor]
        public CatigoriesQuests ( string nameCatigorie, string imgCatigorie, List<string> nameQuest ) {
            this.nameCatigorie = nameCatigorie;
            this.imgCatigorie = imgCatigorie;
            this.nameQuest = nameQuest;
        }
    }
}
