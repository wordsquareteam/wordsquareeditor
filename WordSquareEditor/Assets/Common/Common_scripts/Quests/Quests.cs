﻿using System.Collections.Generic;
using System;

namespace Common {
    public class Quests {
        public string nameLevel;
        public int sizeField;
        public Dictionary<string, List<string>> words;
        public Dictionary<string, List<string>> wordsHints;
        //public List<string> words;
        public List<ArrayIndexMatrinx> indexLetters;

        public Quests (  ) {
            nameLevel = "";
            //words = new List<string>();
            words = new Dictionary<string, List<string>>();
            string[] arrayLanguageEnum = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguageEnum.Length; i++ ) {
                words.Add( arrayLanguageEnum[i], new List<string>() );
            }
            indexLetters = new List<ArrayIndexMatrinx>();
        }

        public Quests ( string nameLevel ) {
            this.nameLevel = nameLevel;
            //words = new List<string>();
            words = new Dictionary<string, List<string>>();
            wordsHints = new Dictionary<string, List<string>>();
            string[] arrayLanguageEnum = Enum.GetNames( typeof( LocalizationManager.Language ) );
            for ( int i = 0; i < arrayLanguageEnum.Length; i++ ) {
                words.Add( arrayLanguageEnum[i], new List<string>() );
                wordsHints.Add( arrayLanguageEnum[i], new List<string>() );
            }
            indexLetters = new List<ArrayIndexMatrinx>();
        }

        public Quests ( Quests newQuests ) {

            sizeField = newQuests.sizeField;

            words = new Dictionary<string, List<string>>();
            //string[] arrayLanguageEnum = Enum.GetNames( typeof( LocalizationManager.Language ) );
            //for ( int i = 0; i < arrayLanguageEnum.Length; i++ ) {
            //    words.Add( arrayLanguageEnum[i], new List<string>() );
            //}

            foreach ( KeyValuePair<string, List<string>> item in newQuests.words ) {
                List<string> wordsTemp = new List<string>();
                for ( int i = 0; i < item.Value.Count; i++ ) {
                    wordsTemp.Add( item.Value[i] );
                }

                words.Add( item.Key, wordsTemp );
            }

            //words = new List<string>();
            //for ( int i = 0; i < newQuests.words.Count; i++ ) {
            //    words.Add( newQuests.words[i] );
            //}

            indexLetters = new List<ArrayIndexMatrinx>();
            for ( int i = 0; i < newQuests.indexLetters.Count; i++ ) {

                indexLetters.Add( new ArrayIndexMatrinx( newQuests.indexLetters[i] ) );
            }
        }

        public void AddNewLetterIndex ( int index, IndexMatrinx newIndexMatrix ) {

            indexLetters[index].AddNewLetterIndex( newIndexMatrix );
        }
    }
}