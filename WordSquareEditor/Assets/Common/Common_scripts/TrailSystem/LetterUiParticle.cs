﻿using UnityEngine;
using UnityEngine.UI;

namespace Puzzle {
    public class LetterUiParticle: MonoBehaviour {
        [SerializeField]
        RectTransform myRectTransform;
        [SerializeField]
        Image myImage;
        [SerializeField]
        Animator myAnimator;

        internal void ActivateParticle ( Vector2 newPosition ) {
            gameObject.SetActive( true );
            myImage.color = new Color( Random.Range( 0.5f, 1f ), Random.Range( 0.5f, 1f ), Random.Range( 0.5f, 1f ) );
            myRectTransform.anchoredPosition = newPosition;
        }
    }
}