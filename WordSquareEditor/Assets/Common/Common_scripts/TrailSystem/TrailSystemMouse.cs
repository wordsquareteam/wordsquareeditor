﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utility;

namespace Puzzle {
    public class TrailSystemMouse: MonoBehaviour {
        [SerializeField]
        RectTransform myRecTransform;
        [SerializeField]
        TrailSystemPool trailSystemPool;

        Coroutine trailSystemMouseUpdate;
        Vector2 oldMousePositionParticle;

        internal void StartTrial () {
            if ( trailSystemMouseUpdate != null ) {
                StopCoroutine( trailSystemMouseUpdate );
                trailSystemMouseUpdate = null;
            }
            Vector2 mousePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRecTransform, Input.mousePosition, Camera.main, out mousePosition );
            oldMousePositionParticle = mousePosition;
            trailSystemMouseUpdate = StartCoroutine( TrailSystemMouseUpdateCoroutine() );
        }

        internal void EndTrial () {
            if ( trailSystemMouseUpdate != null ) {
                StopCoroutine( trailSystemMouseUpdate );
                trailSystemMouseUpdate = null;
            }
        }

        IEnumerator TrailSystemMouseUpdateCoroutine () {
            for ( ; ; ) {
                TrailSystemMouseUpdate();
                yield return null;
            }
        }

        void TrailSystemMouseUpdate () {
            Vector2 mousePosition;
            RectTransformUtility.ScreenPointToLocalPointInRectangle( myRecTransform, Input.mousePosition, Camera.main, out mousePosition );
            if ( Vector2.Distance( oldMousePositionParticle, mousePosition ) < 20f ) {
                return;
            }
            oldMousePositionParticle = mousePosition;
            CreatSquareTrail( mousePosition );
        }

        void CreatSquareTrail ( Vector2 newPosition ) {

            LetterUiParticle letterUiParticle = trailSystemPool.PullLetterUiParticle();
            letterUiParticle.ActivateParticle( newPosition );

            this.Invoke( 0.4f, () => {
                trailSystemPool.PushLetterUiParticle( letterUiParticle );
            } );
        }
    }
}
