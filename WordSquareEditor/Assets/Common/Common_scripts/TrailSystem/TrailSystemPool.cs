﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Puzzle {
    public class TrailSystemPool: MonoBehaviour {
        [SerializeField]
        RectTransform myRecTransform;
        [SerializeField]
        LetterUiParticle letterUiParticlePrf;
        [SerializeField]
        int sizePool;

        List<LetterUiParticle> letterUiParticlePool;
        List<LetterUiParticle> letterUiParticleActive = new List<LetterUiParticle>();

        void Awake () {
            CreatePool();
        }

        void CreatePool () {
            letterUiParticlePool = new List<LetterUiParticle>();

            for ( int i = 0; i < sizePool; i++ ) {
                LetterUiParticle item = CreateParticle();

                letterUiParticlePool.Add( item );
            }
        }

        LetterUiParticle CreateParticle () {
            LetterUiParticle letterUiParticleItem = Instantiate( letterUiParticlePrf, myRecTransform );
            letterUiParticleItem.gameObject.SetActive( false );
            return letterUiParticleItem;
        }

        internal LetterUiParticle PullLetterUiParticle () {
            for ( int i = 0; i < letterUiParticlePool.Count; i++ ) {
                if ( !letterUiParticlePool[i].gameObject.activeSelf ) {
                    letterUiParticleActive.Add( letterUiParticlePool[i] );
                    return letterUiParticlePool[i];
                }
            }

            LetterUiParticle letterUiParticle = CreateParticle();
            letterUiParticleActive.Add( letterUiParticle );
            letterUiParticlePool.Add( letterUiParticle );
            return letterUiParticle;
        }

        internal void PushLetterUiParticle ( LetterUiParticle petterUiParticle ) {
            if ( letterUiParticleActive.Contains( petterUiParticle ) ) {
                letterUiParticleActive.Remove( petterUiParticle );
            }

            if ( letterUiParticlePool.Count >= sizePool ) {
                letterUiParticlePool.Remove( petterUiParticle );
                Destroy( petterUiParticle.gameObject );
                return;
            }

            if ( petterUiParticle == null ) {
                Debug.Break();
            }

            petterUiParticle.gameObject.SetActive( false );
        }

        internal void PushAllLetterUiParticle () {
            List<LetterUiParticle> letterUiParticleActiveTmp = new List<LetterUiParticle>();
            for ( int i = 0; i < letterUiParticleActive.Count; i++ ) {
                letterUiParticleActiveTmp.Add( letterUiParticleActive[i] );
            }

            for ( int i = 0; i < letterUiParticleActiveTmp.Count; i++ ) {
                PushLetterUiParticle( letterUiParticleActiveTmp[i] );
            }

            letterUiParticleActive = new List<LetterUiParticle>();
        }
    }
}
