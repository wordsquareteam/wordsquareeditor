﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Common {
    public class WindowMonoBehaviour: MonoBehaviour {
        [SerializeField]
        CanvasGroup _canvasGroup;

        internal bool IsShown {
            get;
            private set;
        }

        public virtual void Show () {
            _canvasGroup.alpha = 1;
            _canvasGroup.interactable = true;
            _canvasGroup.blocksRaycasts = true;

            SetIsShown( true );
        }

        public virtual void Hide () {
            _canvasGroup.alpha = 0;
            _canvasGroup.interactable = false;
            _canvasGroup.blocksRaycasts = false;

            SetIsShown( false );
        }

        protected void SetIsShown ( bool isShown ) {
            this.IsShown = isShown;
        }
    }
}

