﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent( typeof( EventTrigger ) )]
public class ButtonPress: MonoBehaviour {

    AudioSource myAudioSourse;

    public List<AudioClip> myAudioClip;
    EventTrigger _eventTrigger;

    void Awake () {
        this.gameObject.AddComponent<AudioSource>();
        myAudioSourse = this.gameObject.GetComponent<AudioSource>();
        myAudioSourse.playOnAwake = false;

        _eventTrigger = GetComponent<EventTrigger>();
        if ( _eventTrigger != null ) {
            _eventTrigger = GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerDown;
            entry.callback.AddListener( ( data ) => {
                OnPointerDownDelegate( ( PointerEventData ) data );
            } );
            _eventTrigger.triggers.Add( entry );

            entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener( ( data ) => {
                OnPointerUpDelegate( ( PointerEventData ) data );
            } );
            _eventTrigger.triggers.Add( entry );
        }

        if ( myAudioClip.Count == 0 || myAudioClip == null ) {
            myAudioClip = new List<AudioClip>();
            myAudioClip.Add( Resources.Load<AudioClip>( "Common_sounds/click" ) );
        }
    }

    void OnPointerDownDelegate ( PointerEventData data ) {
        this.gameObject.transform.localPosition = new Vector3( this.gameObject.transform.localPosition.x,
                this.gameObject.transform.localPosition.y - 5f,
                this.gameObject.transform.localPosition.z );

        myAudioSourse.PlayOneShot( myAudioClip[UnityEngine.Random.Range( 0, myAudioClip.Count )] );
    }

    void OnPointerUpDelegate ( PointerEventData data ) {

        this.gameObject.transform.localPosition = new Vector3( this.gameObject.transform.localPosition.x,
                this.gameObject.transform.localPosition.y + 5f,
                this.gameObject.transform.localPosition.z );
    }
}
