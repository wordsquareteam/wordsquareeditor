﻿using UnityEngine;
using Common;
using UnityEngine.UI;

namespace Menu {
    [RequireComponent( typeof( Image ) )]
    public class LockUI: MonoBehaviour {
        [SerializeField]
        Image _lockImage;

        bool _isLock;
        public bool IsLock {
            private set {
                _isLock = value;
            }
            get {
                return _isLock;
            }
        }

        void Start () {
            _isLock = false;
            Lock( false );
        }

        public void Lock ( bool newIsLock ) {
            _lockImage.enabled = newIsLock;
            _isLock = newIsLock;
        }
    }
}