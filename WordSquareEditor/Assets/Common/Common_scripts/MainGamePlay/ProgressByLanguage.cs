﻿using System.Collections.Generic;
using Utility;

namespace Common {

    public class ProgressByLanguage {
        public SecureInt categiries;
        public SecureInt quest;
        public SecureInt numDaylyPuzzleQuest;

        public ProgressByLanguage () {
            categiries = new SecureInt( 0 );
            quest = new SecureInt( 0 );
            numDaylyPuzzleQuest = new SecureInt( 0 );
        }

        public void AddNumDaylyPuzzleQuest () {
            numDaylyPuzzleQuest++;
        }

        public void SetCategiries ( int newCategiries ) {
            categiries = new SecureInt( newCategiries );
        }

        public void SetQuest ( int newQuest ) {
            quest = new SecureInt( newQuest );
        }
    }
}