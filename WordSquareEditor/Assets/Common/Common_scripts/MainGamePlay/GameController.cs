﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Utility;
using Newtonsoft.Json;

namespace Common {
    public class GameController: MonoBehaviour {

        public List<Quests> tempQuests;

        public Quests editQuest;
        public string editQuestName;

        CatigoriesMainAndDailyQuests catigoriesMainAndDailyQuests;

        public DateTime needTime;

        public bool isSound;

        public int currCountShow;
        //public int currCountShow;
        public List<int> countLooseToShowInfo;

        public int randomIntoLevel;
        public int randomCountIntoLevel;

        GameLinks _GL;
        List<List<int>> tottalIndexOfQuest;

        // не уничтожать этот объект в тчении игры
        public void Awake () {

            isSound = true;
            DontDestroyOnLoad( this );
            if ( FindObjectsOfType( GetType() ).Length > 1 ) {

                Destroy( gameObject );
                return;
            }

            Init();
        }

        void Init () {
            _GL = GetComponent<GameLinks>();

            randomIntoLevel = UnityEngine.Random.Range( 25, 45 );
            randomCountIntoLevel = 0;

            currCountShow = 0;
            countLooseToShowInfo = new List<int>() {
                40, 150, 1000
            };

            InitCategoryQuest();
            LocalizationManager.Init();
        }

        void InitCategoryQuest () {
            string pathToQuest = GetPathSavesToLoad( "category_quest" );
            string saveObject = GetJsonFromEncrypterString( pathToQuest );

            catigoriesMainAndDailyQuests = JsonConvert.DeserializeObject<CatigoriesMainAndDailyQuests>( saveObject );

            for ( int i = 0; i < catigoriesMainAndDailyQuests.catigoriesQuests.Count; i++ ) {
                List<int> removeIdex = new List<int>();

                for ( int j = 0; j < catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest.Count; j++ ) {
                    string levelName = catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest[j];

                    if ( !File.Exists( GetPathSavesToLoad( levelName ) ) ) {
                        removeIdex.Add( j );
#if UNITY_EDITOR
                        Debug.Log( "No  such level !!  " + levelName );
#endif
                    }
                }

                for ( int j = 0 ; j < removeIdex.Count; j++ ) {
                    catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest.RemoveAt( removeIdex[j] );
                }
            }

            List<int> indexRemoveDaily = new List<int>();
            for ( int i = 0; i < catigoriesMainAndDailyQuests.daylyPuzzleQuest.Count; i++ ) {
                string levelName = catigoriesMainAndDailyQuests.daylyPuzzleQuest[i];

                if ( !File.Exists( GetPathSavesToLoad( levelName ) ) ) {
                    indexRemoveDaily.Add( i );
#if UNITY_EDITOR
                    Debug.Log( "No  such level !!  " + levelName );
#endif
                }
            }

            for ( int i = 0; i < indexRemoveDaily.Count; i++ ) {
                if ( catigoriesMainAndDailyQuests.daylyPuzzleQuest.Count > 0 && indexRemoveDaily[i] >= 0 &&
                        indexRemoveDaily[i] < catigoriesMainAndDailyQuests.daylyPuzzleQuest.Count ) {
                    catigoriesMainAndDailyQuests.daylyPuzzleQuest.RemoveAt( indexRemoveDaily[i] );
                }
            }

            //высчитываем глобальный индекс квеста. В первой категории 1 - 10, во второй от 11 до 20 и т.д.
            int countIndexQuest = 1;
            tottalIndexOfQuest = new List<List<int>>();
            for ( int i = 0; i < catigoriesMainAndDailyQuests.catigoriesQuests.Count; i++ ) {
                tottalIndexOfQuest.Add( new List<int>() );
                int indexCatigorie = tottalIndexOfQuest.Count - 1;

                for ( int j = 0; j < catigoriesMainAndDailyQuests.catigoriesQuests[i].nameQuest.Count; j++ ) {
                    tottalIndexOfQuest[indexCatigorie].Add( countIndexQuest );
                    countIndexQuest++;
                }
            }
        }

        public bool ISEndGame ( int endEewQuest ) {

            if ( endEewQuest >= catigoriesMainAndDailyQuests.catigoriesQuests.Count ) {

                return true;
            }
            return false;
        }

        public List<CatigoriesQuests> GetCatigoriesQuests () {
            return catigoriesMainAndDailyQuests.catigoriesQuests;
        }

        public List<string> GetDaylyPuzzleQuest () {
            return catigoriesMainAndDailyQuests.daylyPuzzleQuest;
        }

        //public Quests GetTutorialQuest () {
        //    string nameLevel = "tutorial_level";
        //    string fullPathToLevelJson = GetPathSavesToLoad( nameLevel );
        //    return GetQuestsByNameAndPath( nameLevel, fullPathToLevelJson );
        //}

        public Quests GetCurrentQuest () {
            return editQuest;
        }

        //-----------------------------------------------------------------------------------
        public string GetPathSavesToLoad ( string levelName ) {

            string path;
            if ( Application.platform == RuntimePlatform.IPhonePlayer ) {
                path = Application.dataPath + "/Raw/" + levelName + ".json";
            }
            else if ( Application.platform == RuntimePlatform.Android ) {
                //string oriPath = System.IO.Path.Combine( Application.streamingAssetsPath, "/" + levelName + ".json" );
                string oriPath = Application.streamingAssetsPath + "/" + levelName + ".json";

                // Android only use WWW to read file
                WWW reader = new WWW( oriPath );
                while ( !reader.isDone ) { }

                string realPath = Application.persistentDataPath + "/" + levelName + ".json";
                File.WriteAllBytes( realPath, reader.bytes );

                path = realPath;
            }
            else {
                path = Application.dataPath + "/StreamingAssets/" + levelName + ".json";
            }

            return path;
        }

        void Update () {
        }

        public void SaveHintTrue ( int indexQuest, int indexWord, int indexLetter ) {

            tempQuests[indexQuest].indexLetters[indexWord].indexLetters[indexLetter].isHint = true;
        }

        public void SaveHintFalse ( int indexQuest, int indexWord, int indexLetter ) {

            tempQuests[indexQuest].indexLetters[indexWord].indexLetters[indexLetter].isHint = false;
        }

        public void SetEditQuest ( Quests newEditQuest ) {
            editQuest = newEditQuest;

            if ( newEditQuest != null ) {
                editQuestName = newEditQuest.nameLevel;
            }
            else {
                editQuestName = "";
            }
        }

        IEnumerator CountTimeHint () {

            for ( ; ; ) {

                yield return new WaitForEndOfFrame();

                if ( needTime.CompareTo( System.DateTime.Now ) <= -1 ) {

                    break;
                }
            }
        }

        public int GetTotalIndexOfQuest ( int catigories, int quest ) {
            return tottalIndexOfQuest[catigories][quest];
        }

        internal string GetJsonFromEncrypterString ( string savePath ) {
            string saveObject = File.ReadAllText( savePath );
            return AuxiliaryTools.GetJsonFromEncrypterString( saveObject );
        }
    }
}
