﻿using UnityEngine;

namespace Common {
    public class GameLinks: MonoBehaviour {
        [SerializeField]
        ColorDataSet colorDataSet;

        public ColorDataSet ColorDataSet {
            get {
                return colorDataSet;
            }
        }
    }
}
