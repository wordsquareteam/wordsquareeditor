﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Common {
    public class ScreenFixer: MonoBehaviour {
        // Start is called before the first frame update
        void Start () {
            float devider = ( float )Screen.height / ( float )Screen.width;

            if ( devider >= 1.9f ) {
                GetComponent<CanvasScaler>().matchWidthOrHeight = 0;
            }
            else {
                GetComponent<CanvasScaler>().matchWidthOrHeight = 1;
            }
        }
    }
}
